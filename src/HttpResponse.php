<?php
declare (strict_types = 1);

namespace OnionHttp;
use OnionHttp\Cookies;
use OnionHttp\Headers;
use OnionHttp\Response;
use OnionHttp\Stream;
use Psr\Http\Message\StreamInterface;


class HttpResponse extends Response
{

	/**
	 * Create new HTTP response.
	 *
	 * @param int $pnStatus The response status code.
	 * @param array $poHeaders The response headers.
	 * @param StreamInterface|null $poBody The response body.
	 */
	public function __construct (int $pnStatus = 200, array $paHeaders = [], ?StreamInterface $poBody = null)
	{
		$this->nStatus = $this->validStatus($pnStatus);	
		$this->oHeaders = new Headers($paHeaders);
		$this->oCookies = new Cookies();
		$this->oBody = $poBody ? $poBody : new Stream('php://temp', 'w+');
	}
	
	
	/**
	 * This method is applied to the cloned object
	 * after PHP performs an initial shallow-copy. This
	 * method completes a deep-copy by creating new objects
	 * for the cloned object's internal reference pointers.
	 */
	public function __clone ()
	{
		$this->oHeaders = clone $this->oHeaders;
		$this->oCookies = clone $this->oCookies;
	}
    
    
    /**
     * Filter HTTP status code.
     *
     * @param int $pnStatus HTTP status code.
     * @return int
     * @throws \InvalidArgumentException If an invalid HTTP status code is provided.
     */
    protected function validStatus (int $pnStatus) : int
    {
    	if (!is_integer($pnStatus) || $pnStatus<100 || $pnStatus>599)
    	{
    		throw new \InvalidArgumentException('Invalid HTTP status code');
    	}
    	
    	return $pnStatus;
    }
    
    
    /**
     * Write data to the response body.
     *
     * Proxies to the underlying stream and writes the provided data to it.
     *
     * @param string $psData
     * @return \OnionHttp\HttpResponse
     */
    public function write (string $psData) : HttpResponse
    {
    	$this->getBody()->write($psData);
    	
    	return $this;
    }

    
    /**
     * Redirect.
     *
     * This method prepares the response object to return an HTTP Redirect
     * response to the client.
     *
     * @param string|UriInterface $pmUrl The redirect destination.
     * @param int|null $pnStatus The redirect HTTP status code.
     * @return \OnionHttp\HttpResponse
     */
    public function withRedirect (mixed $pmUrl, ?int $pnStatus = null) : HttpResponse
    {
    	$this->withHeader('Location', (string)$pmUrl);
    	
    	if (is_null($pnStatus) && $this->getStatusCode() === 200) 
    	{
    		$pnStatus = 302;
    	}
    	
    	if (!is_null($pnStatus)) 
    	{
    		return $this->withStatus($pnStatus);
    	}
    	
    	return $this;
    }
    
    
    /**
     * Json.
     *
     * This method prepares the response object to return an HTTP Json
     * response to the client.
     *
     * @param mixed $pmData The data
     * @param int $pnStatus The HTTP status code.
     * @param int $pnEncodingOptions Json encoding options
     * @throws \RuntimeException
     * @return \OnionHttp\HttpResponse
     */
    public function withJson (mixed $pmData, ?int $pnStatus = null, int $pnEncodingOptions = 0) : HttpResponse
    {
    	$this->write($lbJson = json_encode($pmData, $pnEncodingOptions));
    	
    	// Ensure that the json encoding passed successfully
    	if ($lbJson === false) 
    	{
    		throw new \RuntimeException(json_last_error_msg(), json_last_error());
    	}
    	
    	$this->withHeader('Content-Type', 'application/json;charset=utf-8');
    	
    	if (isset($pnStatus)) 
    	{
    		return $this->withStatus($pnStatus);
    	}
    	
    	return $this;
    }
    
    
    /**
     * Is this response empty?
     *
     * @return bool
     */
    public function isEmpty () : bool
    {
    	return in_array($this->getStatusCode(), [204, 205, 304]);
    }
    
    
    /**
     * Is this response informational?
     *
     * @return bool
     */
    public function isInformational () : bool
    {
    	return $this->getStatusCode() >= 100 && $this->getStatusCode() < 200;
    }
    
    
    /**
     * Is this response OK?
     *
     * @return bool
     */
    public function isOk () : bool
    {
    	return $this->getStatusCode() === 200;
    }
    
    
    /**
     * Is this response successful?
     *
     * @return bool
     */
    public function isSuccessful () : bool
    {
    	return $this->getStatusCode() >= 200 && $this->getStatusCode() < 300;
    }
    
    
    /**
     * Is this response a redirect?
     *
     * @return bool
     */
    public function isRedirect () : bool
    {
    	return in_array($this->getStatusCode(), [301, 302, 303, 307]);
    }
    
    
    /**
     * Is this response a redirection?
     *
     * @return bool
     */
    public function isRedirection () : bool
    {
    	return $this->getStatusCode() >= 300 && $this->getStatusCode() < 400;
    }
    
    
    /**
     * Is this response forbidden?
     *
     * @return bool
     * @api
     */
    public function isForbidden () : bool
    {
    	return $this->getStatusCode() === 403;
    }
    
    
    /**
     * Is this response not Found?
     *
     * @return bool
     */
    public function isNotFound () : bool
    {
    	return $this->getStatusCode() === 404;
    }
    
    
    /**
     * Is this response a client error?
     *
     * @return bool
     */
    public function isClientError () : bool
    {
    	return $this->getStatusCode() >= 400 && $this->getStatusCode() < 500;
    }
    
    
    /**
     * Is this response a server error?
     *
     * @return bool
     */
    public function isServerError () : bool
    {
    	return $this->getStatusCode() >= 500 && $this->getStatusCode() < 600;
    }
    
    
    /**
     * 
     * @param string $psKey
     * @param mixed $pmValue
     * @return \OnionHttp\Cookies
     */
    public function setCookie (string $psKey, $pmValue) : Cookies
    {
    	$this->oCookies->set($psKey, $pmValue);
    	
    	return $this->oCookies;
    }
    
    
    /**
     *
     * @param array $paCookies
     * @return \OnionHttp\HttpResponse
     */
    public function setCookies (array $paCookies) : HttpResponse
    {
    	if (is_array($paCookies))
    	{
    		foreach ($paCookies as $lsKey => $lmValue)
    		{
    			$this->oCookies->set($lsKey, $lmValue);
    		}
    	}
    	
    	return $this;
    }
    
    
    /**
     * Retrieve cookies.
     *
     * Retrieves cookies sent by the client to the server.
     *
     * The data MUST be compatible with the structure of the $_COOKIE
     * superglobal.
     *
     * @return array
     */
    public function getCookieParams () : array
    {
    	return $this->oCookies->getResponseCookies();
    }
    
    
    /**
     * Fetch cookie value from cookies sent by the client to the server.
     *
     * @param string $psKey The attribute name.
     * @param mixed $pmDefault Default value to return if the attribute does not exist.
     * @return mixed
     */
    public function getCookieParam (string $psKey, mixed $pmDefault = null)
    {
    	return $this->oCookies->getResponseCookie($psKey, $pmDefault);
    }
    
    
    /**
     * 
     */
    public function addCookiesToHeader () : void
    {
    	$laCookies = $this->oCookies->toHeaders();
    	$this->withHeader('Set-Cookie', $laCookies);
    }
    
    
    /**
     * render response to output
     *  
     */
    public function render () : void
    {
    	$laCookies = $this->getCookieParams();
    	
    	if (is_array($laCookies))
    	{
    		foreach ($laCookies as $lsName => $laProp)
    		{
    			if (is_array($laProp))
    			{
    				@setcookie(
    					$lsName,
    					$laProp['value'],
    					$laProp['domain'],
    					$laProp['path'],
    					$laProp['expires'],
    					$laProp['secure'],
    					$laProp['httponly']
    				);
    			}
    		}
    	}
    	
    	$laHeaders = $this->getHeaders();
    	
    	@header(sprintf(
    		'HTTP/%s %s %s',
    		$this->getProtocolVersion(),
    		$this->getStatusCode(),
    		$this->getReasonPhrase()
    	));
    	
    	if (is_array($laHeaders))
    	{
    		foreach ($laHeaders as $lsName => $lmValues)
    		{
    			if (is_array($lmValues))
    			{
    				foreach ($lmValues as $lsValue)
    				{
    					@header("{$lsName}: {$lsValue}");
    				}
    			}
    			else
    			{
    				@header("{$lsName}: {$lmValues}");
    			}
    		}
    	}
    	
   		echo (string)$this->getBody();
    }
    
    
    /**
     * Convert response to string.
     *
     * @return string
     */
    public function __toString () : string
    {
    	$lsOutput = sprintf(
    		'HTTP/%s %s %s',
    		$this->getProtocolVersion(),
    		$this->getStatusCode(),
    		$this->getReasonPhrase()
    	);
    	
    	$lsOutput .= Response::EOL;
    	
    	$this->addCookiesToHeader();
    	
    	foreach ($this->getHeaders() as $lsName => $lmValues) 
    	{
    		if (is_array($lmValues))
    		{
    			foreach ($lmValues as $lsValue)
    			{
    				$lsOutput .= sprintf('%s: %s', $lsName, $lsValue) . Response::EOL;
    			}
    		}
    	}
    	
    	$lsOutput .= Response::EOL;
    	
    	$lsOutput .= (string)$this->getBody();
    	
    	return $lsOutput;
    }
}
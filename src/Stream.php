<?php
declare (strict_types = 1);

namespace OnionHttp;
use Psr\Http\Message\StreamInterface;


/**
 * Describes a data stream.
 *
 * Typically, an instance will wrap a PHP stream; this interface provides
 * a wrapper around the most common operations, including serialization of
 * the entire stream to a string.
 */
class Stream implements StreamInterface
{
	/**
	 * Bit mask to determine if the stream is a pipe
	 */
	const FSTAT_MODE = 0010000;
	
	/**
	 * Resource modes
	 *
	 * @var  array
	 */
	protected static $aModes = [
		'readable' => ['r', 'r+', 'w+', 'a+', 'x+', 'c+'],
		'writable' => ['r+', 'w', 'w+', 'a', 'a+', 'x', 'x+', 'c', 'c+'],
	];
	
	/**
	 * The underlying stream resource
	 *
	 * @var resource
	 */
	protected $rStream = null;
	
	/**
	 * Stream metadata
	 *
	 * @var array
	 */
	protected $aMeta = null;
	
	/**
	 * Is this stream readable?
	 *
	 * @var bool
	 */
	protected $bReadable = null;
	
	/**
	 * Is this stream writable?
	 *
	 * @var bool
	 */
	protected $bWritable = null;
	
	/**
	 * Is this stream seekable?
	 *
	 * @var bool
	 */
	protected $bSeekable = null;
	
	/**
	 * The size of the stream if known
	 *
	 * @var null|int
	 */
	protected $nSize = null;
	
	/**
	 * Is this stream a pipe?
	 *
	 * @var bool
	 */
	protected $bIsPipe = null;
	
	
	/**
	 * Create a new Stream.
	 *
	 * @param string|resource $pmStream An url or a file path or a PHP resource handle.
	 * @param string $psMode
	 * @throws InvalidArgumentException If argument is not a resource.
	 */
	public function __construct (mixed $pmStream = null, string $psMode = 'r')
	{
		if (is_null($pmStream))
		{
			$pmStream = fopen('php://temp', 'w+');
			stream_copy_to_stream(fopen('php://input', 'r'), $pmStream);
			rewind($pmStream);
		}
		elseif (is_string($pmStream) && !empty($pmStream))
		{
				$pmStream = fopen($pmStream, $psMode);
		}
		
		if (!is_resource($pmStream))
		{
			throw new \InvalidArgumentException(__METHOD__ . ' argument must be a valid PHP resource');
		}
		
		$this->detach();
		$this->rStream = $pmStream;
		
		return $this;
	}

	
    /**
     * Reads all data from the stream into a string, from the beginning to end.
     *
     * This method MUST attempt to seek to the beginning of the stream before
     * reading data and read the stream until the end is reached.
     *
     * Warning: This could attempt to load a large amount of data into memory.
     *
     * This method MUST NOT raise an exception in order to conform with PHP's
     * string casting operations.
     *
     * @see http://php.net/manual/en/language.oop5.magic.php#object.tostring
     * @return string
     */
    public function __toString () : string
    {
    	if (is_resource($this->rStream))
    	{
	    	try 
	    	{
	    		$this->rewind();
	    		return $this->getContents();
	    	} 
	    	catch (\RuntimeException $e) 
	    	{
	    		return '';
	    	}
    	}
    	
    	return '';
    }

    
    /**
     * Closes the stream and any underlying resources.
     *
     * @return void
     */
    public function close () : void
    {
    	if (is_resource($this->rStream)) 
    	{
    		if ($this->isPipe()) 
    		{
    			@pclose($this->rStream);
    		} 
    		else 
    		{
    			@fclose($this->rStream);
    		}
    	}
    	
    	$this->detach();
    }
    
    
    /**
     * Separates any underlying resources from the stream.
     *
     * After the stream has been detached, the stream is in an unusable state.
     *
     * @return resource|null Underlying PHP stream, if any
     */
    public function detach ()
    {
    	$lrResource = null;
    	
    	if (is_resource($this->rStream))
    	{
    		$lrResource = $this->rStream;
    		$this->rStream = null;
    		$this->aMeta = null;
    		$this->bReadable = null;
    		$this->bWritable = null;
    		$this->bSeekable = null;
    		$this->nSize = null;
    		$this->bIsPipe = null;
    	}
    	
    	return $lrResource;
    }
    
    
    /**
     * Get the size of the stream if known.
     *
     * @return int|null Returns the size in bytes if known, or null if unknown.
     */
    public function getSize () : int
    {
    	if (!$this->nSize && is_resource($this->rStream)) 
    	{
    		$laStats = fstat($this->rStream);
    		$this->nSize = isset($laStats['size']) && !$this->isPipe() ? $laStats['size'] : null;
    	}
    	
    	return $this->nSize;
    }
    
    
    /**
     * Returns the current position of the file read/write pointer
     *
     * @return int Position of the file pointer
     * @throws \RuntimeException on error.
     */
    public function tell () : int
    {
    	if (!is_resource($this->rStream)|| ($lnPosition = ftell($this->rStream)) === false || $this->isPipe()) 
    	{
    		throw new \RuntimeException('Could not get the position of the pointer in stream');
    	}
    	
    	return $lnPosition;
    }
    
    
    /**
     * Returns true if the stream is at the end of the stream.
     *
     * @return bool
     */
    public function eof () : bool
    {
    	return is_resource($this->rStream) ? feof($this->rStream) : true;
    }
    
    
    /**
     * Returns whether or not the stream is seekable.
     *
     * @return bool
     */
    public function isSeekable () : bool
    {    	
    	if ($this->bSeekable === null) 
    	{
    		$this->bSeekable = false;
    		
    		if (is_resource($this->rStream)) 
    		{
    			$laMeta = $this->getMetadata();
    			$this->bSeekable = !$this->isPipe() && $laMeta['seekable'];
    		}
    	}
    	
    	return $this->bSeekable;
    }
    
    
    /**
     * Seek to a position in the stream.
     *
     * @link http://www.php.net/manual/en/function.fseek.php
     * @param int $pnOffset Stream offset
     * @param int $pnWhence Specifies how the cursor position will be calculated
     *     based on the seek offset. Valid values are identical to the built-in
     *     PHP $whence values for `fseek()`.  SEEK_SET: Set position equal to
     *     offset bytes SEEK_CUR: Set position to current location plus offset
     *     SEEK_END: Set position to end-of-stream plus offset.
     * @throws \RuntimeException on failure.
     */
    public function seek ($pnOffset, $pnWhence = SEEK_SET) : void
    {
    	// Note that fseek returns 0 on success!
    	if (!$this->isSeekable() || fseek($this->rStream, $pnOffset, $pnWhence) === -1) 
    	{
    		throw new \RuntimeException('Could not seek in stream');
    	}
    }
    
    
    /**
     * Seek to the beginning of the stream.
     *
     * If the stream is not seekable, this method will raise an exception;
     * otherwise, it will perform a seek(0).
     *
     * @see seek()
     * @link http://www.php.net/manual/en/function.fseek.php
     * @throws \RuntimeException on failure.
     */
    public function rewind () : void
    {
    	if (!$this->isSeekable() || rewind($this->rStream) === false) 
    	{
    		throw new \RuntimeException('Could not rewind stream');
    	}
    }
    
    
    /**
     * Returns whether or not the stream is writable.
     *
     * @return bool
     */
    public function isWritable () : bool
    {
    	if ($this->bWritable === null) 
    	{
    		$this->bWritable = false;
    		
    		if (is_resource($this->rStream)) 
    		{
    			$laMeta= $this->getMetadata();
    			
    			foreach (self::$aModes['writable'] as $lsMode) 
    			{
    				if (strpos($laMeta['mode'], $lsMode) === 0) 
    				{
    					$this->bWritable = true;
    					break;
    				}
    			}
    		}
    	}
    	
    	return $this->bWritable;
    }
    
    
    /**
     * Write data to the stream.
     *
     * @param string $psString The string that is to be written.
     * @return int Returns the number of bytes written to the stream.
     * @throws \RuntimeException on failure.
     */
    public function write ($psString) : int
    {
    	if (!$this->isWritable() || ($lnWritten = fwrite($this->rStream, $psString)) === false) 
    	{
    		throw new \RuntimeException('Could not write to stream');
    	}
    	
    	// reset size so that it will be recalculated on next call to getSize()
    	$this->nSize = null;
    	
    	return $lnWritten;
    }
    
    
    /**
     * Returns whether or not the stream is readable.
     *
     * @return bool
     */
    public function isReadable () : bool
    {
    	if ($this->bReadable === null)
    	{
    		if ($this->isPipe())
    		{
    			$this->bReadable = true;
    		}
    		else
    		{
    			$this->bReadable = false;
    			
    			if (is_resource($this->rStream))
    			{
    				$laMeta = $this->getMetadata();
    				
    				foreach (self::$aModes['readable'] as $lsMode)
    				{
    					if (strpos($laMeta['mode'], $lsMode) === 0)
    					{
    						$this->bReadable = true;
    						break;
    					}
    				}
    			}
    		}
    	}
    	
    	return $this->bReadable;
    }
    
    
    /**
     * Read data from the stream.
     *
     * @param int $pnLength Read up to $length bytes from the object and return
     *     them. Fewer than $length bytes may be returned if underlying stream
     *     call returns fewer bytes.
     * @return string Returns the data read from the stream, or an empty string
     *     if no bytes are available.
     * @throws \RuntimeException if an error occurs.
     */
    public function read ($pnLength) : string
    {
    	if (!$this->isReadable() || ($lsData = fread($this->rStream, $pnLength)) === false) 
    	{
    		throw new \RuntimeException('Could not read from stream');
    	}
    	
    	return $lsData;
    }
    
    
    /**
     * Returns the remaining contents in a string
     *
     * @return string
     * @throws \RuntimeException if unable to read or an error occurs while
     *     reading.
     */
    public function getContents () : string
    {
    	if (!$this->isReadable() || ($lsContents = stream_get_contents($this->rStream)) === false) 
    	{
    		throw new \RuntimeException('Could not get contents of stream');
    	}
    	
    	return $lsContents;
    }
    
    
    /**
     * Get stream metadata as an associative array or retrieve a specific key.
     *
     * The keys returned are identical to the keys returned from PHP's
     * stream_get_meta_data() function.
     *
     * @link http://php.net/manual/en/function.stream-get-meta-data.php
     * @param string $psKey Specific metadata to retrieve.
     * @return array|mixed|null Returns an associative array if no key is
     *     provided. Returns a specific key value if a key is provided and the
     *     value is found, or null if the key is not found.
     */
    public function getMetadata (?string $psKey = null)
    {
    	if (null !== $this->rStream)
    	{
    		$this->aMeta = stream_get_meta_data($this->rStream);
	    	
	    	if (null !== $psKey)
	    	{
	    		if (key_exists($psKey, $this->aMeta))
	    		{
	    			return $this->aMeta[$psKey];
	    		}
	    	}
	    	else 
	    	{
	    		return $this->aMeta;
	    	}
    	}
    	
    	return null;
    }
    
    
    ######## not in psr7 #########
    
    
    /**
     * Returns whether or not the stream is a pipe.
     *
     * @return bool
     */
    public function isPipe () : bool
    {
    	if ($this->bIsPipe === null)
    	{
    		$this->bIsPipe = false;
    		
    		if (is_resource($this->rStream))
    		{
    			$laStats = fstat($this->rStream);
    			$this->bIsPipe = ($laStats['mode'] & self::FSTAT_MODE) !== 0;
    		}
    	}
    	
    	return $this->bIsPipe;
    }
}
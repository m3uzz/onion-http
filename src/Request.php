<?php
declare (strict_types = 1);

namespace OnionHttp;
use OnionHttp\Message;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;


/**
 * Representation of an outgoing, client-side request.
 *
 * Per the HTTP specification, this interface includes properties for
 * each of the following:
 *
 * - Protocol version
 * - HTTP method
 * - URI
 * - Headers
 * - Message body
 *
 * During construction, implementations MUST attempt to set the Host header from
 * a provided URI if no Host header is provided.
 *
 * Requests are considered immutable; all methods that might change state MUST
 * be implemented such that they retain the internal state of the current
 * message and return an instance that contains the changed state.
 */
class Request extends Message implements RequestInterface
{
	/**
	 * Valid HTTP methods
	 *
	 * @var array
	 */
	protected static $aValidMethods = [
		'GET' => true,
		'POST' => true,
		'PUT' => true,
		'DELETE' => true,
		'OPTIONS' => true,
		'HEAD' => true,
		'PURGE' => true,
		'LOCK' => true,
		'UNLOCK' => true,
	];
			
	/**
	 * The request method
	 *
	 * @var string
	 */
	protected $sMethod;
	
	/**
	 * The request URI object
	 *
	 * @var \Psr\Http\Message\UriInterface
	 */
	protected $oUri;
	
	/**
	 * The request URI target (path + query string)
	 *
	 * @var string
	 */
	protected $sRequestTarget;
		
	/**
	 * The server environment variables at the time the request was created.
	 *
	 * @var array
	 */
	protected $aServerParams;
	
	/**
	 * The request attributes (route segment names and values)
	 *
	 * @var \OnionLib\Collection
	 */
	protected $oAttributes;
	
	/**
	 * List of uploaded files
	 *
	 * @var array of UploadedFileInterface
	 */
	protected $aUploadedFiles;
	
	
	/**
	 * Retrieves the message's request target.
	 *
	 * Retrieves the message's request-target either as it will appear (for
	 * clients), as it appeared at request (for servers), or as it was
	 * specified for the instance (see withRequestTarget()).
	 *
	 * In most cases, this will be the origin-form of the composed URI,
	 * unless a value was provided to the concrete implementation (see
	 * withRequestTarget() below).
	 *
	 * If no URI is available, and no request-target has been specifically
	 * provided, this method MUST return the string "/".
	 *
	 * @return string
	 */
	public function getRequestTarget () : string
	{
		if ($this->sRequestTarget)
		{
			return $this->sRequestTarget;
		}
		elseif (!is_null($this->oUri))
		{
			$lsPath = $this->oUri->getPath();
			$lsPath = '/' . ltrim($lsPath, '/');
			
			$lsQuery = $this->oUri->getQuery();
			
			if (!empty($lsQuery))
			{
				$lsPath .= '?' . $lsQuery;
			}
			
			$this->sRequestTarget = $lsPath;
			
			return $this->sRequestTarget;
		}
		
		return '/';
	}
	
	
	/**
	 * Return an instance with the specific request-target.
	 *
	 * If the request needs a non-origin-form request-target — e.g., for
	 * specifying an absolute-form, authority-form, or asterisk-form —
	 * this method may be used to create an instance with the specified
	 * request-target, verbatim.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * changed request target.
	 *
	 * @link http://tools.ietf.org/html/rfc7230#section-2.7 (for the various
	 *     request-target forms allowed in request messages)
	 * @param mixed $pmRequestTarget
	 * @return \OnionHttp\Request
	 * @throws \InvalidArgumentException if the request target is invalid
	 */
	public function withRequestTarget ($pmRequestTarget) : Request
	{
		if (preg_match('#\s#', $pmRequestTarget))
		{
			throw new \InvalidArgumentException(
					'Invalid request target provided; must be a string and cannot contain whitespace'
					);
		}
		
		$this->sRequestTarget = $pmRequestTarget;
		
		return $this;
	}

	
	/**
	 * Retrieves the HTTP method of the request.
	 *
	 * @return string Returns the request method.
	 */
	public function getMethod () : string
	{
		return $this->sMethod;
	}
	
	
	/**
	 * Return an instance with the provided HTTP method.
	 *
	 * While HTTP method names are typically all uppercase characters, HTTP
	 * method names are case-sensitive and thus implementations SHOULD NOT
	 * modify the given string.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * changed request method.
	 *
	 * @param string $psMethod Case-sensitive method.
	 * @return \OnionHttp\Request
	 * @throws \InvalidArgumentException for invalid HTTP methods.
	 */
	public function withMethod ($psMethod) : Request
	{
		$psMethod = $this->validMethod($psMethod);

		$this->sMethod = $psMethod;
		
		return $this;
	}
	
	
	/**
	 * Retrieves the URI instance.
	 *
	 * This method MUST return a UriInterface instance.
	 *
	 * @link http://tools.ietf.org/html/rfc3986#section-4.3
	 * @return \OnionHttp\Uri Returns a UriInterface instance
	 *     representing the URI of the request.
	 */
	public function getUri () : Uri
	{
		return $this->oUri;
	}
	
	
	/**
	 * Returns an instance with the provided URI.
	 *
	 * This method MUST update the Host header of the returned request by
	 * default if the URI contains a host component. If the URI does not
	 * contain a host component, any pre-existing Host header MUST be carried
	 * over to the returned request.
	 *
	 * You can opt-in to preserving the original state of the Host header by
	 * setting `$preserveHost` to `true`. When `$preserveHost` is set to
	 * `true`, this method interacts with the Host header in the following ways:
	 *
	 * - If the the Host header is missing or empty, and the new URI contains
	 *   a host component, this method MUST update the Host header in the returned
	 *   request.
	 * - If the Host header is missing or empty, and the new URI does not contain a
	 *   host component, this method MUST NOT update the Host header in the returned
	 *   request.
	 * - If a Host header is present and non-empty, this method MUST NOT update
	 *   the Host header in the returned request.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * new UriInterface instance.
	 *
	 * @link http://tools.ietf.org/html/rfc3986#section-4.3
	 * @param UriInterface $poUri New request URI to use.
	 * @param bool $pbPreserveHost Preserve the original state of the Host header.
	 * @return \OnionHttp\Request
	 */
	public function withUri (UriInterface $poUri, $pbPreserveHost = false) : Request
	{
		$this->oUri = $poUri;
		
		if (!$pbPreserveHost)
		{
			if ($poUri->getHost() !== '')
			{
				$this->oHeaders->set('Host', $poUri->getHost());
			}
		}
		else
		{
			if ($poUri->getHost() !== '' && (!$this->hasHeader('Host') || $this->getHeaderLine('Host') === ''))
			{
				$this->oHeaders->set('Host', $poUri->getHost());
			}
		}
		
		return $this;
	}
	
	
	######## not in psr7 #########
	
	
	/**
	 * Validate the HTTP method
	 *
	 * @param string $psMethod
	 * @return string
	 * @throws \InvalidArgumentException on invalid HTTP method.
	 */
	protected function validMethod (string $psMethod) : string
	{
		$psMethod = strToUpper($psMethod);
		
		if (!isset(static::$aValidMethods[$psMethod])) 
		{
			throw new \InvalidArgumentException("Unsupported HTTP method '{$psMethod}' provided");
		}
		
		return $psMethod;
	}
}
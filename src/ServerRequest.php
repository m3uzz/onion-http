<?php
declare (strict_types = 1);

namespace OnionHttp;
use OnionHttp\Request;
use OnionLib\Collection;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;


/**
 * Representation of an incoming, server-side HTTP request.
 *
 * Per the HTTP specification, this interface includes properties for
 * each of the following:
 *
 * - Protocol version
 * - HTTP method
 * - URI
 * - Headers
 * - Message body
 *
 * Additionally, it encapsulates all data as it has arrived to the
 * application from the CGI and/or PHP environment, including:
 *
 * - The values represented in $_SERVER.
 * - Any cookies provided (generally via $_COOKIE)
 * - Query string arguments (generally via $_GET, or as parsed via parse_str())
 * - Upload files, if any (as represented by $_FILES)
 * - Deserialized body parameters (generally from $_POST)
 *
 * $_SERVER values MUST be treated as immutable, as they represent application
 * state at the time of request; as such, no methods are provided to allow
 * modification of those values. The other values provide such methods, as they
 * can be restored from $_SERVER or the request body, and may need treatment
 * during the application (e.g., body parameters may be deserialized based on
 * content type).
 *
 * Additionally, this interface recognizes the utility of introspecting a
 * request to derive and match additional parameters (e.g., via URI path
 * matching, decrypting cookie values, deserializing non-form-encoded body
 * content, matching authorization headers to users, etc). These parameters
 * are stored in an "attributes" property.
 *
 * Requests are considered immutable; all methods that might change state MUST
 * be implemented such that they retain the internal state of the current
 * message and return an instance that contains the changed state.
 */
class ServerRequest extends Request implements ServerRequestInterface
{
	/**
	 * The request query string params
	 *
	 * @var \OnionLib\Collection
	 */
	protected $oQueryParams = null;
	
	/**
	 * The request post params
	 *
	 * @var \OnionLib\Collection
	 */
	protected $oPostParams = null;
	
	/**
	 * The args params
	 *
	 * @var \OnionLib\Collection
	 */
	protected $oArgvParams = null;
	
	/**
	 * The request body parsed (if possible) into a PHP array or object
	 *
	 * @var \OnionLib\Collection
	 */
	protected $oBodyParsed = null;
	
	/**
	 * List of request body parsers (e.g., url-encoded, JSON, XML, multipart)
	 *
	 * @var array of callable
	 */
	protected $cBodyParsers = [];
	
	
    /**
     * Retrieve server parameters.
     *
     * Retrieves data related to the incoming request environment,
     * typically derived from PHP's $_SERVER superglobal. The data IS NOT
     * REQUIRED to originate from $_SERVER.
     *
     * @return array
     */
    public function getServerParams () : array
    {
    	return $this->aServerParams;
    }
    
    
    /**
     * Retrieve cookies.
     *
     * Retrieves cookies sent by the client to the server.
     *
     * The data MUST be compatible with the structure of the $_COOKIE
     * superglobal.
     *
     * @return array
     */
    public function getCookieParams () : array
    {
    	return $this->oCookies->getRequestCookies();
    }

    
    /**
     * Return an instance with the specified cookies.
     *
     * The data IS NOT REQUIRED to come from the $_COOKIE superglobal, but MUST
     * be compatible with the structure of $_COOKIE. Typically, this data will
     * be injected at instantiation.
     *
     * This method MUST NOT update the related Cookie header of the request
     * instance, nor related values in the server params.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated cookie values.
     *
     * @param array $paCookies Array of key/value pairs representing cookies.
     * @return \OnionHttp\ServerRequest
     */
    public function withCookieParams (array $paCookies) : ServerRequest
    {
    	$this->aCookies = $paCookies;
    	
    	return $this;
    }

    
    /**
     * Retrieve query string arguments.
     *
     * Retrieves the deserialized query string arguments, if any.
     *
     * Note: the query params might not be in sync with the URI or server
     * params. If you need to ensure you are only getting the original
     * values, you may need to parse the query string from `getUri()->getQuery()`
     * or from the `QUERY_STRING` server param.
     *
     * @return array
     */
    public function getQueryParams () : array
    {
    	if (is_object($this->oQueryParams))
    	{
    		return $this->oQueryParams->all();
    	}
    	elseif (!is_null($this->oUri))
    	{
    		$lsQuery = rawurldecode($this->oUri->getQuery());
    		
    		if (mb_parse_str($lsQuery, $laQuery))
    		{    		
    			$this->oQueryParams = new Collection();
    			$this->oQueryParams->populate($laQuery);
    	
    			return $this->oQueryParams->all();
    		}
    	}
    	
    	return [];
    }
    

    /**
     * Return an instance with the specified query string arguments.
     *
     * These values SHOULD remain immutable over the course of the incoming
     * request. They MAY be injected during instantiation, such as from PHP's
     * $_GET superglobal, or MAY be derived from some other value such as the
     * URI. In cases where the arguments are parsed from the URI, the data
     * MUST be compatible with what PHP's parse_str() would return for
     * purposes of how duplicate query parameters are handled, and how nested
     * sets are handled.
     *
     * Setting query string arguments MUST NOT change the URI stored by the
     * request, nor the values in the server params.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated query string arguments.
     *
     * @param array $paQuery Array of query string arguments, typically from
     *     $_GET.
     * @return \OnionHttp\ServerRequest
     */
    public function withQueryParams (array $paQuery) : ServerRequest
    {
    	$this->oQueryParams = new Collection($paQuery);
    	
    	return $this;
    }
    
    
    /**
     * Retrieve normalized file upload data.
     *
     * This method returns upload metadata in a normalized tree, with each leaf
     * an instance of Psr\Http\Message\UploadedFileInterface.
     *
     * These values MAY be prepared from $_FILES or the message body during
     * instantiation, or MAY be injected via withUploadedFiles().
     *
     * @return array An array tree of UploadedFileInterface instances; an empty
     *     array MUST be returned if no data is present.
     */
    public function getUploadedFiles () : array
    {
    	return $this->aUploadedFiles;
    }

    
    /**
     * Create a new instance with the specified uploaded files.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated body parameters.
     *
     * @param array $paUploadedFiles An array tree of UploadedFileInterface instances.
     * @return \OnionHttp\ServerRequest
     * @throws \InvalidArgumentException if an invalid structure is provided.
     */
    public function withUploadedFiles (array $paUploadedFiles) : ServerRequest
    {
    	$this->aUploadedFiles = $paUploadedFiles;
    	
    	return $this;
    }

    
    /**
     * Retrieve any parameters provided in the request body.
     *
     * If the request Content-Type is either application/x-www-form-urlencoded
     * or multipart/form-data, and the request method is POST, this method MUST
     * return the contents of $_POST.
     *
     * Otherwise, this method may return any results of deserializing
     * the request body content; as parsing returns structured content, the
     * potential types MUST be arrays or objects only. A null value indicates
     * the absence of body content.
     *
     * @return null|array|object The deserialized body parameters, if any.
     *     These will typically be an array or object.
     */
    public function getParsedBody ()
    {
    	if (!is_null($this->oBodyParsed))
    	{
    		return $this->oBodyParsed;
    	}
    	elseif ($this->oBody instanceof StreamInterface)
    	{	
	    	$lsMediaType = $this->getMediaType();
	    	
	    	// look for a media type with a structured syntax suffix (RFC 6839)
	    	$laMediaTypes = explode('+', (string)$lsMediaType);
	    	
	    	if (count($laMediaTypes) >= 2)
	    	{
	    		$lsMediaType = 'application/' . array_pop($laMediaTypes);
	    	}
	    	
	    	if (isset($this->cBodyParsers[$lsMediaType]))
	    	{
	    		$lsBody = (string)$this->getBody();
	    		
	    		if (is_callable($this->cBodyParsers[$lsMediaType]))
	    		{
	    			$lmBodyParsed = $this->cBodyParsers[$lsMediaType]($lsBody);
	    		
	    			if (!is_null($lmBodyParsed) && !is_object($lmBodyParsed) && !is_array($lmBodyParsed))
	    			{
	    				throw new \RuntimeException(
	    					'Request body media type parser return value must be an array, an object, or null'
	    				);
	    			}
	    		
	    			$this->oBodyParsed = $lmBodyParsed;
	    		
	    			return $this->oBodyParsed;
	    		}
	    	}
    	}
    	
    	return null;
    }

    
    /**
     * Return an instance with the specified body parameters.
     *
     * These MAY be injected during instantiation.
     *
     * If the request Content-Type is either application/x-www-form-urlencoded
     * or multipart/form-data, and the request method is POST, use this method
     * ONLY to inject the contents of $_POST.
     *
     * The data IS NOT REQUIRED to come from $_POST, but MUST be the results of
     * deserializing the request body content. Deserialization/parsing returns
     * structured data, and, as such, this method ONLY accepts arrays or objects,
     * or a null value if nothing was available to parse.
     *
     * As an example, if content negotiation determines that the request data
     * is a JSON payload, this method could be used to create a request
     * instance with the deserialized parameters.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated body parameters.
     *
     * @param null|array|object $pmData The deserialized body data. This will
     *     typically be in an array or object.
     * @return \OnionHttp\ServerRequest
     * @throws \InvalidArgumentException if an unsupported argument type is
     *     provided.
     */
    public function withParsedBody ($pmData) : ServerRequest
    {
    	if (is_array($pmData))
    	{
    		$this->oBodyParsed = new Collection($pmData);
    	}
    	elseif (is_object($pmData))
    	{
    		$this->oBodyParsed = $pmData;
    	}
    	elseif (is_null($pmData))
    	{
    		$this->oBodyParsed = null;
    	}
    	else
    	{
    		throw new \InvalidArgumentException('Parsed body value must be an array, an object, or null');
    	}
    	
    	return $this;
    }
    
    
    /**
     * Retrieve attributes derived from the request.
     *
     * The request "attributes" may be used to allow injection of any
     * parameters derived from the request: e.g., the results of path
     * match operations; the results of decrypting cookies; the results of
     * deserializing non-form-encoded message bodies; etc. Attributes
     * will be application and request specific, and CAN be mutable.
     *
     * @return array Attributes derived from the request.
     */
    public function getAttributes () : array
    {
    	return $this->oAttributes->all();
    }

    
    /**
     * Retrieve a single derived request attribute.
     *
     * Retrieves a single derived request attribute as described in
     * getAttributes(). If the attribute has not been previously set, returns
     * the default value as provided.
     *
     * This method obviates the need for a hasAttribute() method, as it allows
     * specifying a default value to return if the attribute is not found.
     *
     * @see getAttributes()
     * @param string $psName The attribute name.
     * @param mixed $pmDefault Default value to return if the attribute does not exist.
     * @return mixed
     */
    public function getAttribute (string $psName, mixed $pmDefault = null)
    {
    	return $this->oAttributes->get($psName, $pmDefault);
    }

    
    /**
     * Return an instance with the specified derived request attribute.
     *
     * This method allows setting a single derived request attribute as
     * described in getAttributes().
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated attribute.
     *
     * @see getAttributes()
     * @param string $psName The attribute name.
     * @param mixed $pmValue The value of the attribute.
     * @return \OnionHttp\ServerRequest
     */
    public function withAttribute ($psName, $pmValue) : ServerRequest
    {
    	$this->oAttributes->set($psName, $pmValue);
    	
    	return $this;
    }

    
    /**
     * Return an instance that removes the specified derived request attribute.
     *
     * This method allows removing a single derived request attribute as
     * described in getAttributes().
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that removes
     * the attribute.
     *
     * @see getAttributes()
     * @param string $psName The attribute name.
     * @return \OnionHttp\ServerRequest
     */
    public function withoutAttribute ($psName) : ServerRequest
    {
    	$this->oAttributes->remove($psName);
    	
    	return $this;
    }

    
    ######## not in psr7 #########
	
	
	/**
	 * Get request content type.
	 *
	 * @return string|null The request content type, if known
	 */
	public function getContentType ()
	{
		$laResult = $this->getHeader('Content-Type');
		
		return $laResult ? $laResult[0] : null;
    }
    	
	
	/**
	 * Get request media type, if known.
	 *
	 * @return string|null The request media type, minus content-type params
	 */
	public function getMediaType ()
	{
		$lsContentType = $this->getContentType();
		
		if ($lsContentType)
		{
			$laContentTypeParts = preg_split('/\s*[;,]\s*/', $lsContentType);
			
			return strtolower($laContentTypeParts[0]);
		}
		
		return null;
    }
        
    
    /**
     * Register media type parser.
     *
     * @param string $psMediaType A HTTP media type (excluding content-type params).
     * @param callable|null $pcCallable  A callable that returns parsed contents for media type.
     */
    public function registerMediaTypeParser (string $psMediaType, ?callable $pcCallable = null) : void
    {
    	if ($pcCallable instanceof \Closure)
    	{
    		$pcCallable = $pcCallable->bindTo($this);
    	}
    	
    	$this->cBodyParsers[(string)$psMediaType] = $pcCallable;
    }
}
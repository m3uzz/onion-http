<?php
declare (strict_types = 1);

namespace OnionHttp;


/**
 * Session helper
 */
class Session
{
	/**
	 * Session from HTTP request
	 *
	 * @var array
	 */
	protected $aRequestSession = [];
	
	/**
	 * Session for HTTP response
	 *
	 * @var array
	 */
	protected $aResponseSession = [];
	
	/**
	 *
	 * @var array
	 */
	protected $aDefaults = [];


	/**
	 * Create new session helper
	 *
	 * @param array $paSession
	 */
	public function __construct (array $paSession = [])
	{
		$this->aRequestSession = $paSession;
	}
	
	
	/**
	 * 
	 * @return \OnionHttp\Session
	 */
	public static function create () : Session
	{
		return new static($_SESSION);
	}
	
	
	/**
	 * Get request cookie
	 *
	 * @param string $psName Session name
	 * @param mixed $pmDefault Session default value
	 * @return mixed Session value if present, else default
	 */
	public function getRequestSession (string $psName, mixed $pmDefault = null)
	{
		return isset($this->aRequestSession[$psName]) ? $this->aRequestSession[$psName] : $pmDefault;
	}
	
	
	/**
	 * Get all request session
	 *
	 * @return array
	 */
	public function getRequestSessionAll () : array
	{
		return $this->aRequestSession;
	}
	
	
	/**
	 * Set response session
	 *
	 * @param string $psName Session name
	 * @param string|array $pmValue Session value, or session properties
	 * @return \OnionHttp\Session
	 */
	public function set (string $psName, $pmValue) : Session
	{
		if (!is_array($pmValue)) 
		{
			$pmValue = ['value' => (string)$pmValue];
		}
		
		$this->aResponseSession[$psName] = array_replace($this->aDefaults, $pmValue);
		
		return $this;
	}
	
	
	/**
	 * Get response session
	 *
	 * @param string $psName Session name
	 * @param mixed $pmDefault Session default value
	 * @return mixed Session value if present
	 */
	public function getResponseSession (string $psName, mixed $pmDefault = null)
	{
		return isset($this->aResponseSession[$psName]) ? $this->aResponseSession[$psName] : $pmDefault;
	}
	
	
	/**
	 * Get all response session
	 *
	 * @return array of Session
	 */
	public function getResponseSessionAll () : array
	{
		return $this->aResponseSession;
	}
	
	
	/**
	 * 
	 * @param string|null $psName
	 */
	public function register (?string $psName = null) : void
	{
		if (!is_null($psName))
		{
			$laSession[$psName] = $this->getResponseSession($psName);
		}
		else 
		{
			$laSession = $this->getResponseSessionAll();
		}

		if (is_array($laSession))
		{
			foreach ($laSession as $lsName => $laProp)
			{
				if (is_array($laProp))
				{
					setcookie(
						$lsName,
						$laProp['value'],
						$laProp['expires'],
						$laProp['path'],
						$laProp['domain'],						
						$laProp['secure'],
						$laProp['httponly']
					);
				}
			}
		}
	}
}
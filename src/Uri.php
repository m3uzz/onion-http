<?php
declare (strict_types = 1);

namespace OnionHttp;
use Psr\Http\Message\UriInterface;


/**
 * Value object representing a URI.
 *
 * This interface is meant to represent URIs according to RFC 3986 and to
 * provide methods for most common operations. Additional functionality for
 * working with URIs can be provided on top of the interface or externally.
 * Its primary use is for HTTP requests, but may also be used in other
 * contexts.
 *
 * Instances of this interface are considered immutable; all methods that
 * might change state MUST be implemented such that they retain the internal
 * state of the current instance and return an instance that contains the
 * changed state.
 *
 * Typically the Host header will be also be present in the request message.
 * For server-side requests, the scheme will typically be discoverable in the
 * server parameters.
 *
 * @link http://tools.ietf.org/html/rfc3986 (the URI specification)
 */
class Uri implements UriInterface
{
	/**
	 * Schemes permited in a Uri
	 * 
	 * @var array
	 */
	protected $aAcceptedSchemes = [
			'' => null, 
			'https' => 443, 
			'http' => 80,
			'ftp' => 21,
			'sftp' => 22,
			'ftps' => 22,
			'ssh' => 22,
			'smtp' => 25,
			'imap' => 143
	];
	
	/**
	 * Uri scheme (without "://" suffix)
	 *
	 * @var string
	 */
	protected $sScheme = '';
	
	/**
	 * Uri user
	 *
	 * @var string
	 */
	protected $sUser = '';
	
	/**
	 * Uri password
	 *
	 * @var string
	 */
	protected $sPassword = '';
	
	/**
	 * Uri host
	 *
	 * @var string
	 */
	protected $sHost = '';
	
	/**
	 * Uri port number
	 *
	 * @var null|int
	 */
	protected $nPort;
	
	/**
	 * Uri path
	 *
	 * @var string
	 */
	protected $sPath = '';
	
	/**
	 * Uri query string (without "?" prefix)
	 *
	 * @var string
	 */
	protected $sQuery = '';
	
	/**
	 * Uri fragment string (without "#" prefix)
	 *
	 * @var string
	 */
	protected $sFragment = '';
	
	
    /**
     * Retrieve the scheme component of the URI.
     *
     * If no scheme is present, this method MUST return an empty string.
     *
     * The value returned MUST be normalized to lowercase, per RFC 3986
     * Section 3.1.
     *
     * The trailing ":" character is not part of the scheme and MUST NOT be
     * added.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-3.1
     * @return string The URI scheme.
     */
    public function getScheme () : string
    {
    	return $this->sScheme;
    }
    
    
    /**
     * Retrieve the authority component of the URI.
     *
     * If no authority information is present, this method MUST return an empty
     * string.
     *
     * The authority syntax of the URI is:
     *
     * <pre>
     * [user-info@]host[:port]
     * </pre>
     *
     * If the port component is not set or is the standard port for the current
     * scheme, it SHOULD NOT be included.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-3.2
     * @return string The URI authority, in "[user-info@]host[:port]" format.
     */
    public function getAuthority () : string
    {
    	$userInfo = $this->getUserInfo();
    	$host = $this->getHost();
    	$port = $this->getPort();
    	
    	return ($userInfo ? $userInfo . '@' : '') . $host . ($port !== null ? ':' . $port : '');
    }
    
    
    /**
     * Retrieve the user information component of the URI.
     *
     * If no user information is present, this method MUST return an empty
     * string.
     *
     * If a user is present in the URI, this will return that value;
     * additionally, if the password is also present, it will be appended to the
     * user value, with a colon (":") separating the values.
     *
     * The trailing "@" character is not part of the user information and MUST
     * NOT be added.
     *
     * @return string The URI user information, in "username[:password]" format.
     */
    public function getUserInfo () : string
    {
    	return $this->sUser . ($this->sPassword ? ':' . $this->sPassword : '');
    }
    
    
    /**
     * Retrieve the host component of the URI.
     *
     * If no host is present, this method MUST return an empty string.
     *
     * The value returned MUST be normalized to lowercase, per RFC 3986
     * Section 3.2.2.
     *
     * @see http://tools.ietf.org/html/rfc3986#section-3.2.2
     * @return string The URI host.
     */
    public function getHost () : string
    {
    	return $this->sHost;
    }
    
    
    /**
     * Retrieve the port component of the URI.
     *
     * If a port is present, and it is non-standard for the current scheme,
     * this method MUST return it as an integer. If the port is the standard port
     * used with the current scheme, this method SHOULD return null.
     *
     * If no port is present, and no scheme is present, this method MUST return
     * a null value.
     *
     * If no port is present, but a scheme is present, this method MAY return
     * the standard port for that scheme, but SHOULD return null.
     *
     * @return null|int The URI port.
     */
    public function getPort () : int
    {
    	return $this->nPort && !$this->hasStandardPort() ? $this->nPort : null;
    }
    
    
    /**
     * Retrieve the path component of the URI.
     *
     * The path can either be empty or absolute (starting with a slash) or
     * rootless (not starting with a slash). Implementations MUST support all
     * three syntaxes.
     *
     * Normally, the empty path "" and absolute path "/" are considered equal as
     * defined in RFC 7230 Section 2.7.3. But this method MUST NOT automatically
     * do this normalization because in contexts with a trimmed base path, e.g.
     * the front controller, this difference becomes significant. It's the task
     * of the user to handle both "" and "/".
     *
     * The value returned MUST be percent-encoded, but MUST NOT double-encode
     * any characters. To determine what characters to encode, please refer to
     * RFC 3986, Sections 2 and 3.3.
     *
     * As an example, if the value should include a slash ("/") not intended as
     * delimiter between path segments, that value MUST be passed in encoded
     * form (e.g., "%2F") to the instance.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-2
     * @see https://tools.ietf.org/html/rfc3986#section-3.3
     * @return string The URI path.
     */
    public function getPath () : string
    {
    	return $this->sPath;
    }
    
    
    /**
     * Retrieve the query string of the URI.
     *
     * If no query string is present, this method MUST return an empty string.
     *
     * The leading "?" character is not part of the query and MUST NOT be
     * added.
     *
     * The value returned MUST be percent-encoded, but MUST NOT double-encode
     * any characters. To determine what characters to encode, please refer to
     * RFC 3986, Sections 2 and 3.4.
     *
     * As an example, if a value in a key/value pair of the query string should
     * include an ampersand ("&") not intended as a delimiter between values,
     * that value MUST be passed in encoded form (e.g., "%26") to the instance.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-2
     * @see https://tools.ietf.org/html/rfc3986#section-3.4
     * @return string The URI query string.
     */
    public function getQuery () : string
    {
    	return $this->sQuery;
    }
    
    
    /**
     * Retrieve the fragment component of the URI.
     *
     * If no fragment is present, this method MUST return an empty string.
     *
     * The leading "#" character is not part of the fragment and MUST NOT be
     * added.
     *
     * The value returned MUST be percent-encoded, but MUST NOT double-encode
     * any characters. To determine what characters to encode, please refer to
     * RFC 3986, Sections 2 and 3.5.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-2
     * @see https://tools.ietf.org/html/rfc3986#section-3.5
     * @return string The URI fragment.
     */
    public function getFragment () : string
    {
    	return $this->sFragment;
    }
    
    
    /**
     * Return an instance with the specified scheme.
     *
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified scheme.
     *
     * Implementations MUST support the schemes "http" and "https" case
     * insensitively, and MAY accommodate other schemes if required.
     *
     * An empty scheme is equivalent to removing the scheme.
     *
     * @param string $psScheme The scheme to use with the new instance.
     * @return \OnionHttp\Uri A new instance with the specified scheme.
     * @throws \InvalidArgumentException for invalid or unsupported schemes.
     */
    public function withScheme ($psScheme) : Uri
    {
    	$psScheme = $this->validScheme($psScheme);

    	$this->sScheme = $psScheme;
    	
    	return $this;
    }
    
    
    /**
     * Return an instance with the specified user information.
     *
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified user information.
     *
     * Password is optional, but the user information MUST include the
     * user; an empty string for the user is equivalent to removing user
     * information.
     *
     * @param string $psUser The user name to use for authority.
     * @param null|string $psPassword The password associated with $user.
     * @return \OnionHttp\Uri A new instance with the specified user information.
     */
    public function withUserInfo (string $psUser, ?string $psPassword = null) : Uri
    {
    	$this->sUser = $psUser;
    	$this->sPassword = $psPassword ? $psPassword: '';
    	
    	return $this;
    }
    
    
    /**
     * Return an instance with the specified host.
     *
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified host.
     *
     * An empty host value is equivalent to removing the host.
     *
     * @param string $psHost The hostname to use with the new instance.
     * @return \OnionHttp\Uri A new instance with the specified host.
     * @throws \InvalidArgumentException for invalid hostnames.
     */
    public function withHost ($psHost) : Uri
    {
    	$this->sHost = $psHost;
    	
    	return $this;
    }
    
    
    /**
     * Return an instance with the specified port.
     *
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified port.
     *
     * Implementations MUST raise an exception for ports outside the
     * established TCP and UDP port ranges.
     *
     * A null value provided for the port is equivalent to removing the port
     * information.
     *
     * @param null|int $pnPort The port to use with the new instance; a null value
     *     removes the port information.
     * @return \OnionHttp\Uri A new instance with the specified port.
     * @throws \InvalidArgumentException for invalid ports.
     */
    public function withPort ($pnPort) : Uri
    {
    	$pnPort = $this->validPort($pnPort);
    	$this->nPort = $pnPort;
    	
    	return $this;
    }
    
    
    /**
     * Return an instance with the specified path.
     *
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified path.
     *
     * The path can either be empty or absolute (starting with a slash) or
     * rootless (not starting with a slash). Implementations MUST support all
     * three syntaxes.
     *
     * If the path is intended to be domain-relative rather than path relative then
     * it must begin with a slash ("/"). Paths not starting with a slash ("/")
     * are assumed to be relative to some base path known to the application or
     * consumer.
     *
     * Users can provide both encoded and decoded path characters.
     * Implementations ensure the correct encoding as outlined in getPath().
     *
     * @param string $psPath The path to use with the new instance.
     * @return \OnionHttp\Uri A new instance with the specified path.
     * @throws \InvalidArgumentException for invalid paths.
     */
    public function withPath ($psPath) : Uri
    {
    	if (!is_string($psPath)) 
    	{
    		throw new \InvalidArgumentException('Uri path must be a string');
    	}
    	
    	$this->sPath = $this->validPath($psPath);
    	
    	return $this;
    }
    
    
    /**
     * Return an instance with the specified query string.
     *
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified query string.
     *
     * Users can provide both encoded and decoded query characters.
     * Implementations ensure the correct encoding as outlined in getQuery().
     *
     * An empty query string value is equivalent to removing the query string.
     *
     * @param string $psQuery The query string to use with the new instance.
     * @return \OnionHttp\Uri A new instance with the specified query string.
     * @throws \InvalidArgumentException for invalid query strings.
     */
    public function withQuery ($psQuery) : Uri
    {
    	if (!is_string($psQuery) && !method_exists($psQuery, '__toString')) 
    	{
    		throw new \InvalidArgumentException('Uri query must be a string');
    	}
    	
    	$psQuery = ltrim((string)$psQuery, '?');

    	$this->sQuery = $this->validQuery($psQuery);
    	
    	return $this;
    }
    
    
    /**
     * Return an instance with the specified URI fragment.
     *
     * This method MUST retain the state of the current instance, and return
     * an instance that contains the specified URI fragment.
     *
     * Users can provide both encoded and decoded fragment characters.
     * Implementations ensure the correct encoding as outlined in getFragment().
     *
     * An empty fragment value is equivalent to removing the fragment.
     *
     * @param string $psFragment The fragment to use with the new instance.
     * @return \OnionHttp\Uri A new instance with the specified fragment.
     */
    public function withFragment ($psFragment) : Uri
    {
    	if (!is_string($psFragment) && !method_exists($psFragment, '__toString')) 
    	{
    		throw new \InvalidArgumentException('Uri fragment must be a string');
    	}
    	
    	$psFragment = ltrim((string)$psFragment, '#');

    	$this->sFragment = $this->validQuery($psFragment);
    	
    	return $this;
    }
    
    
    /**
     * Return the string representation as a URI reference.
     *
     * Depending on which components of the URI are present, the resulting
     * string is either a full URI or relative reference according to RFC 3986,
     * Section 4.1. The method concatenates the various components of the URI,
     * using the appropriate delimiters:
     *
     * - If a scheme is present, it MUST be suffixed by ":".
     * - If an authority is present, it MUST be prefixed by "//".
     * - The path can be concatenated without delimiters. But there are two
     *   cases where the path has to be adjusted to make the URI reference
     *   valid as PHP does not allow to throw an exception in __toString():
     *     - If the path is rootless and an authority is present, the path MUST
     *       be prefixed by "/".
     *     - If the path is starting with more than one "/" and no authority is
     *       present, the starting slashes MUST be reduced to one.
     * - If a query is present, it MUST be prefixed by "?".
     * - If a fragment is present, it MUST be prefixed by "#".
     *
     * @see http://tools.ietf.org/html/rfc3986#section-4.1
     * @return string
     */
    public function __toString ()
    {
    	$lsScheme = $this->getScheme();
    	$lsAuthority = $this->getAuthority();
    	$lsPath = $this->getPath();
    	$lsQuery = $this->getQuery();
    	$lsFragment = $this->getFragment();
    	
    	$lsUrl = ($lsScheme ? "{$lsScheme}:" : '');
    	$lsUrl .= ($lsAuthority ? "//{$lsAuthority}" : '');
    	$lsUrl .= '/' . ltrim($lsPath, '/');
    	$lsUrl .= ($lsQuery ? "?{$lsQuery}" : '');
    	$lsUrl .= ($lsFragment ? "#{$lsFragment}" : '');
    	
    	return $lsUrl;
    }
    
    
    ######## not in psr7 #########
    
    
    /**
     *
     * @param string|array $pmUri
     * @return \OnionHttp\Uri
     */
    public function __construct (mixed $pmUri = null)
    {
    	if (is_string($pmUri))
    	{
    		return $this->createByString($pmUri);
    	}
    	elseif (is_array($pmUri) || is_null($pmUri))
    	{
    		return $this->createByArray($pmUri);
    	}
    }
    
    
    /**
     *
     * @throws \Exception
     * @return array int array object
     */
    public function getProperties () : array
    {
    	$laProperties = get_object_vars($this);
    	
    	unset($laProperties['aAcceptedSchemes']);

    	return $laProperties;
    }
    
    
    /**
     * Create new Uri.
     *
     * @param string $psScheme Uri scheme.
     * @param string $psHost Uri host.
     * @param int|null $pnPort Uri port number.
     * @param string $psPath Uri path.
     * @param string $psQuery Uri query string.
     * @param string $psFragment Uri fragment.
     * @param string $psUser Uri user.
     * @param string $psPassword Uri password.
     * @return \OnionHttp\Uri
     */
    public function setUri (string $psScheme, string $psHost, ?int $pnPort = null, string $psPath = '/', string $psQuery = '', string $psFragment = '', string $psUser = '', string $psPassword = '') : Uri
    {
    	$this->sScheme = $this->validScheme($psScheme);
    	$this->sHost = $psHost;
    	$this->nPort = $this->validPort($pnPort);
    	$this->sPath = empty($psPath) ? '/' : $this->validPath($psPath);
    	$this->sQuery = $this->validQuery($psQuery);
    	$this->sFragment = $this->validQuery($psFragment);
    	$this->sUser = $psUser;
    	$this->sPassword = $psPassword;
    	
    	return $this;
    }
    
    
    /**
     * Create new Uri from string.
     *
     * @param string $psUri Complete Uri string
     *     (i.e., https://user:pass@host:443/path?query).
     * @return \OnionHttp\Uri
     */
    public function createByString (string $psUri) : Uri
    {
    	if (!is_string($psUri) && !method_exists($psUri, '__toString'))
    	{
    		throw new \InvalidArgumentException('Uri must be a string');
    	}
    	
    	$laUri = parse_url($psUri);
    	$lsScheme = (isset($laUri['scheme']) ? strtolower($laUri['scheme']) : '');
    	$lsUser = (isset($laUri['user']) ? $laUri['user'] : '');
    	$lsPass = (isset($laUri['pass']) ? $laUri['pass'] : '');
    	$lsHost = (isset($laUri['host']) ? $laUri['host'] : '');
    	$lnPortDefault = (isset($this->aAcceptedSchemes[$lsScheme]) ? $this->aAcceptedSchemes[$lsScheme] : null);
    	$lnPort = (isset($laUri['port']) ? $laUri['port'] : $lnPortDefault);
    	$lsPath = (isset($laUri['path']) ? $laUri['path'] : '');
    	$lsQuery = (isset($laUri['query']) ? $laUri['query'] : '');
    	$lsFragment = (isset($laUri['fragment']) ? $laUri['fragment'] : '');
    	
    	return $this->setUri($lsScheme, $lsHost, $lnPort, $lsPath, $lsQuery, $lsFragment, $lsUser, $lsPass);
    }
    
    
    /**
     * Create new Uri from environment
     *
     * @param array|null $paData
     * @return \OnionHttp\Uri
     */
    public function createByArray (?array $paData = null) : Uri
    {
    	if (is_null($paData))
    	{
    		$paData = $_SERVER;
    	}
    	
    	$lsScheme = (isset($paData['REQUEST_SCHEME']) ? strtolower($paData['REQUEST_SCHEME']) : '');
    	$lsUsername = (isset($paData['PHP_AUTH_USER']) ? $paData['PHP_AUTH_USER'] : '');
    	$lsPassword = (isset($paData['PHP_AUTH_PW']) ? $paData['PHP_AUTH_USER'] : '');
    	$lsHost = (isset($paData['HTTP_HOST']) ? $paData['HTTP_HOST'] : '');
    	$lnPortDefault = (isset($this->aAcceptedSchemes[$lsScheme]) ? $this->aAcceptedSchemes[$lsScheme] : null);
    	$lnPort = (isset($paData['SERVER_PORT']) ? (int)$paData['SERVER_PORT'] : $lnPortDefault);
    	$lsRequestUri = (isset($paData['REQUEST_URI']) ? $paData['REQUEST_URI'] : '');
    	
    	$laUri = parse_url("http://{$lsHost}{$lsRequestUri}");
    	
    	$lsHost = (isset($laUri['host']) ? $laUri['host'] : '');
    	$lsPath = (isset($laUri['path']) ? $laUri['path'] : '');
    	$lsQuery = (isset($laUri['query']) ? rawurlencode($laUri['query']) : '');
    	$laHeaders = getAllHeaders();
    	$lsFragment = (isset($laHeaders['FRAGMENT_STRING']) ? $laHeaders['FRAGMENT_STRING'] : '');
    	
    	return $this->setUri($lsScheme, $lsHost, $lnPort, $lsPath, $lsQuery, $lsFragment, $lsUsername, $lsPassword);
    }
    
    
    /**
     * Return the fully qualified base URL.
     *
     * Note that this method never includes a trailing /
     *
     * @return string
     */
    public function getBaseUrl () : string
    {
    	$lsScheme = $this->getScheme();
    	$lsAuthority = $this->getAuthority();
    	
    	$lsBaseUrl = ($lsScheme ? "{$lsScheme}:" : '');
    	$lsBaseUrl .= ($lsAuthority ? "//{$lsAuthority}" : '');
    	
    	return $lsBaseUrl;
    }
    
    
    /**
     * Does this Uri use a standard port?
     *
     * @return bool
     */
    protected function hasStandardPort () : bool
    {
    	return ($this->sScheme === 'http' && $this->nPort === 80) || ($this->sScheme === 'https' && $this->nPort === 443);
    }
    
    
    /**
     * Filter Uri port.
     *
     * @param null|int $pnPort The Uri port number.
     * @return null|int
     * @throws \InvalidArgumentException If the port is invalid.
     */
    protected function validPort (?int $pnPort) : int
    {
    	if (is_null($pnPort) || (is_integer($pnPort) && ($pnPort >= 1 && $pnPort <= 65535)))
    	{
    		return $pnPort;
    	}
    	
    	throw new \InvalidArgumentException('Uri port must be null or an integer between 1 and 65535 (inclusive)');
    }
    
    
    /**
     * Filter Uri path.
     *
     * This method percent-encodes all reserved
     * characters in the provided path string. This method
     * will NOT double-encode characters that are already
     * percent-encoded.
     *
     * @param string $psPath The raw uri path.
     * @return string The RFC 3986 percent-encoded uri path.
     * @link http://www.faqs.org/rfcs/rfc3986.html
     */
    protected function validPath (string $psPath) : string
    {
    	return preg_replace_callback (
    		'/(?:[^a-zA-Z0-9_\-\.~:@&=\+\$,\/;%]+|%(?![A-Fa-f0-9]{2}))/',
    		function ($laMatch)
    		{
    			return rawurlencode($laMatch[0]);
    		},
    		$psPath
    	);
    }
    
    
    /**
     * Filter Uri scheme.
     *
     * @param string $psScheme Raw Uri scheme.
     * @return string
     * @throws \InvalidArgumentException If the Uri scheme is not a string.
     * @throws \InvalidArgumentException If Uri scheme is not "", "https", or "http".
     */
    protected function validScheme (string $psScheme) : string
    {
    	if (!is_string($psScheme) && !method_exists($psScheme, '__toString'))
    	{
    		throw new \InvalidArgumentException('Uri scheme must be a string');
    	}
    	
    	$psScheme = str_replace('://', '', strtolower((string)$psScheme));
    	
    	if (!isset($this->aAcceptedSchemes[$psScheme]))
    	{
    		throw new \InvalidArgumentException('Uri scheme must be one of: "", "https", "http"');
    	}
    	
    	return $psScheme;
    }
    
    
    /**
     * Filters the query string or fragment of a URI.
     *
     * @param string $psQuery The raw uri query string.
     * @return string The percent-encoded query string.
     */
    protected function validQuery (string $psQuery) : string
    {
    	return preg_replace_callback(
    		'/(?:[^a-zA-Z0-9_\-\.~!\$&\'\(\)\*\+,;=%:@\/\?]+|%(?![A-Fa-f0-9]{2}))/',
    		function ($laMatch) {
    			return rawurlencode($laMatch[0]);
    		},
    		$psQuery
    	);
    }
}
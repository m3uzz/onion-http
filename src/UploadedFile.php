<?php
declare (strict_types = 1);

namespace OnionHttp;
use OnionHttp\Stream;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;


/**
 * Value object representing a file uploaded through an HTTP request.
 *
 * Instances of this interface are considered immutable; all methods that
 * might change state MUST be implemented such that they retain the internal
 * state of the current instance and return an instance that contains the
 * changed state.
 */
class UploadedFile implements UploadedFileInterface
{
	/**
	 * The client-provided full path to the file
	 *
	 * @var string
	 */
	protected $sFile;
	
	/**
	 * The client-provided file name.
	 *
	 * @var string
	 */
	protected $sName;
	
	/**
	 * The client-provided media type of the file.
	 *
	 * @var string
	 */
	protected $sType;
	
	/**
	 * The size of the file in bytes.
	 *
	 * @var int
	 */
	protected $nSize;
	
	/**
	 * A valid PHP UPLOAD_ERR_xxx code for the file upload.
	 *
	 * @var int
	 */
	protected $nError = UPLOAD_ERR_OK;
	
	/**
	 * Indicates if the upload is from a SAPI environment.
	 *
	 * @var bool
	 */
	protected $bSApi = false;
	
	/**
	 * An optional StreamInterface wrapping the file resource.
	 *
	 * @var StreamInterface
	 */
	protected $oStream;
	
	/**
	 * Indicates if the uploaded file has already been moved.
	 *
	 * @var bool
	 */
	protected $bMoved = false;
	
	
	/**
	 * set a new UploadedFile instance.
	 *
	 * @param string      $psFile The full path to the uploaded file provided by the client.
	 * @param string|null $psName The file name.
	 * @param string|null $psType The file media type.
	 * @param int|null    $pnSize The file size in bytes.
	 * @param int         $pnError The UPLOAD_ERR_XXX code representing the status of the upload.
	 * @param bool        $pbSApi Indicates if the upload is in a SAPI environment.
	 */
	public function __construct (string $psFile, ?string $psName = null, ?string $psType = null, ?int $pnSize = null, int $pnError = UPLOAD_ERR_OK, bool $pbSApi = false)
	{
		$this->sFile = $psFile;
		$this->sName = $psName;
		$this->sType = $psType;
		$this->nSize = $pnSize;
		$this->nError = $pnError;
		$this->bSApi = $pbSApi;
		
		return $this;
	}
	
	
	/**
	 * Retrieve a stream representing the uploaded file.
	 *
	 * This method MUST return a StreamInterface instance, representing the
	 * uploaded file. The purpose of this method is to allow utilizing native PHP
	 * stream functionality to manipulate the file upload, such as
	 * stream_copy_to_stream() (though the result will need to be decorated in a
	 * native PHP stream wrapper to work with such functions).
	 *
	 * If the moveTo() method has been called previously, this method MUST raise
	 * an exception.
	 *
	 * @return StreamInterface Stream representation of the uploaded file.
	 * @throws \RuntimeException in cases when no stream is available or can be
	 *         created.
	 */
	public function getStream () : StreamInterface
	{
		if ($this->bMoved) 
		{
			throw new \RuntimeException(sprintf('Uploaded file %1s has already been moved', $this->sName));
		}
		
		if ($this->oStream === null) 
		{
			$this->oStream = new Stream($this->sFile);
		}
		
		return $this->oStream;
	}
	
	
	/**
	 * Move the uploaded file to a new location.
	 *
	 * Use this method as an alternative to move_uploaded_file(). This method is
	 * guaranteed to work in both SAPI and non-SAPI environments.
	 * Implementations must determine which environment they are in, and use the
	 * appropriate method (move_uploaded_file(), rename(), or a stream
	 * operation) to perform the operation.
	 *
	 * $targetPath may be an absolute path, or a relative path. If it is a
	 * relative path, resolution should be the same as used by PHP's rename()
	 * function.
	 *
	 * The original file or stream MUST be removed on completion.
	 *
	 * If this method is called more than once, any subsequent calls MUST raise
	 * an exception.
	 *
	 * When used in an SAPI environment where $_FILES is populated, when writing
	 * files via moveTo(), is_uploaded_file() and move_uploaded_file() SHOULD be
	 * used to ensure permissions and upload status are verified correctly.
	 *
	 * If you wish to move to a stream, use getStream(), as SAPI operations
	 * cannot guarantee writing to stream destinations.
	 *
	 * @see http://php.net/is_uploaded_file
	 * @see http://php.net/move_uploaded_file
	 * @param string $psTargetPath Path to which to move the uploaded file.
	 * @throws \InvalidArgumentException if the $path specified is invalid.
	 * @throws \RuntimeException on any error during the move operation, or on
	 *         the second or subsequent call to the method.
	 */
	public function moveTo ($psTargetPath) : void
	{
		if ($this->bMoved) 
		{
			throw new \RuntimeException('Uploaded file already moved');
		}
		
		$lbTargetIsStream = strpos($psTargetPath, '://') > 0;
		
		if (!$lbTargetIsStream && !is_writable(dirname($psTargetPath))) 
		{
			throw new \InvalidArgumentException('Upload target path is not writable');
		}
		
		if ($lbTargetIsStream) 
		{
			if (!copy($this->sFile, $psTargetPath)) 
			{
				throw new \RuntimeException(sprintf('Error moving uploaded file %1s to %2s', $this->sName, $psTargetPath));
			}
			
			if (!unlink($this->sFile)) 
			{
				throw new \RuntimeException(sprintf('Error removing uploaded file %1s', $this->sName));
			}
		} 
		elseif ($this->bSApi) 
		{
			if (!is_uploaded_file($this->sFile)) 
			{
				throw new \RuntimeException(sprintf('%1s is not a valid uploaded file', $this->sFile));
			}
			
			if (!move_uploaded_file($this->sFile, $psTargetPath)) 
			{
				throw new \RuntimeException(sprintf('Error moving uploaded file %1s to %2s', $this->sName, $psTargetPath));
			}
		} 
		else 
		{
			if (!rename($this->sFile, $psTargetPath)) 
			{
				throw new \RuntimeException(sprintf('Error moving uploaded file %1s to %2s', $this->sName, $psTargetPath));
			}
		}
		
		$this->bMoved = true;
	}
	
	
	/**
	 * Retrieve the file size.
	 *
	 * Implementations SHOULD return the value stored in the "size" key of
	 * the file in the $_FILES array if available, as PHP calculates this based
	 * on the actual size transmitted.
	 *
	 * @return int|null The file size in bytes or null if unknown.
	 */
	public function getSize () : ?int
	{
		return $this->nSize;
	}
	
	
	/**
	 * Retrieve the error associated with the uploaded file.
	 *
	 * The return value MUST be one of PHP's UPLOAD_ERR_XXX constants.
	 *
	 * If the file was uploaded successfully, this method MUST return
	 * UPLOAD_ERR_OK.
	 *
	 * Implementations SHOULD return the value stored in the "error" key of
	 * the file in the $_FILES array.
	 *
	 * @see http://php.net/manual/en/features.file-upload.errors.php
	 * @return int One of PHP's UPLOAD_ERR_XXX constants.
	 */
	public function getError () : int
	{
		return $this->nError;
	}
	
	
	/**
	 * Retrieve the filename sent by the client.
	 *
	 * Do not trust the value returned by this method. A client could send
	 * a malicious filename with the intention to corrupt or hack your
	 * application.
	 *
	 * Implementations SHOULD return the value stored in the "name" key of
	 * the file in the $_FILES array.
	 *
	 * @return string|null The filename sent by the client or null if none
	 *         was provided.
	 */
	public function getClientFilename () : ?string
	{
		return $this->sName;
	}
	
	
	/**
	 * Retrieve the media type sent by the client.
	 *
	 * Do not trust the value returned by this method. A client could send
	 * a malicious media type with the intention to corrupt or hack your
	 * application.
	 *
	 * Implementations SHOULD return the value stored in the "type" key of
	 * the file in the $_FILES array.
	 *
	 * @return string|null The media type sent by the client or null if none
	 *         was provided.
	 */
	public function getClientMediaType () : ?string
	{
		return $this->sType;
	}
	
	
	######## not in psr7 #########
	
	
	/**
	 * Parse a non-normalized, i.e. $_FILES superglobal, tree of uploaded file data.
	 *
	 * @param array $paUploadedFiles The non-normalized tree of uploaded file data.
	 * @return array A normalized tree of UploadedFile instances.
	 */
	public static function factory (array $paUploadedFiles) : array
	{
		$laFiles = [];
		
		$paUploadedFiles = self::normalizeUploadedFiles($paUploadedFiles);
		
		foreach ($paUploadedFiles as $lsField => $laUploadedFile)
		{
			$laFiles[$lsField] = new static(
				$laUploadedFile['tmp_name'],
				isset($laUploadedFile['name']) ? $laUploadedFile['name'] : null,
				isset($laUploadedFile['type']) ? $laUploadedFile['type'] : null,
				isset($laUploadedFile['size']) ? $laUploadedFile['size'] : null,
				$laUploadedFile['error'],
				true
			);
		}
		
		return $laFiles;
	}
	
	
	/**
	 * Parse a non-normalized, i.e. $_FILES superglobal, tree of uploaded file data.
	 *
	 * @param array $paUploadedFiles The non-normalized tree of uploaded file data.
	 * @return array A normalized tree of $_FILES.
	 */
	public static function normalizeUploadedFiles (array $paUploadedFiles) : array
	{
		$laUploadedFiles = [];
		
		foreach ($paUploadedFiles as $lsField => $laUploadedFile)
		{
			if (!isset($laUploadedFile['error']))
			{
				throw new \InvalidArgumentException('The array passed is not a $_FILES tree.');
			}
			elseif (is_array($laUploadedFile['error']))
			{
				foreach ($laUploadedFile['error'] as $lsIndex => $lmError)
				{
					// normalise subarray and re-parse to move the input's keyname up a level
					$lsNewName = "{$lsField}-{$lsIndex}";
					$laUploadedFiles[$lsNewName]['name'] = $laUploadedFile['name'][$lsIndex];
					$laUploadedFiles[$lsNewName]['type'] = $laUploadedFile['type'][$lsIndex];
					$laUploadedFiles[$lsNewName]['tmp_name'] = $laUploadedFile['tmp_name'][$lsIndex];
					$laUploadedFiles[$lsNewName]['error'] = $laUploadedFile['error'][$lsIndex];
					$laUploadedFiles[$lsNewName]['size'] = $laUploadedFile['size'][$lsIndex];
				}
			}
			else
			{
				$laUploadedFiles[$lsField] = $laUploadedFile;
			}
		}
		
		return $laUploadedFiles;
	}
}
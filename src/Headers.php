<?php
declare (strict_types = 1);

namespace OnionHttp;
use OnionLib\ArrayObject;
use OnionLib\Collection;


/**
 * Headers
 *
 * This class represents a collection of HTTP headers
 * that is used in both the HTTP request and response objects.
 * It also enables header name case-insensitivity when
 * getting or setting a header value.
 *
 * Each HTTP header can have multiple values. This class
 * stores values into an array for each header name. When
 * you request a header value, you receive an array of values
 * for that header.
 */
class Headers extends Collection
{
	/**
	 * Special HTTP headers that do not have the "HTTP_" prefix
	 *
	 * @var array
	 */
	protected static $aSpecial = [
			'CONTENT_TYPE' => true,
			'CONTENT_LENGTH' => true,
			'PHP_AUTH_USER' => true,
			'PHP_AUTH_PW' => true,
			'PHP_AUTH_DIGEST' => true,
			'AUTH_TYPE' => true,
	];
	
	
	/**
	 * Create new headers collection with data extracted from
	 * the PHP global environment
	 *
	 * @param array|null $paData Global server variables
	 * @return self
	 */
	public function __construct (?array $paData = null)
	{
		if (is_null($paData))
		{
			$paData = $_SERVER;
		}
		
		$laData = [];
		$paData = $this->determineAuthorization($paData);

		foreach ($paData as $lsKey => $lmValue) 
		{
			$lsKey = strtoupper($lsKey);
			
			if (isset(static::$aSpecial[$lsKey]) || strpos($lsKey, 'HTTP_') === 0) 
			{
				if ($lsKey !== 'HTTP_CONTENT_LENGTH') 
				{
					$laData[$lsKey] = $lmValue;
				}
			}
		}
		
		return parent::__construct($laData);
	}
	
	
	/**
	 * If HTTP_AUTHORIZATION does not exist tries to get it from
	 * getallheaders() when available.
	 *
	 * @param array $paData
	 * @return array
	 */
	public function determineAuthorization (array $paData) : array
	{
		$lsAuthorization = isset($paData['HTTP_AUTHORIZATION']) ? $paData['HTTP_AUTHORIZATION'] : null;
		
		if (empty($lsAuthorization) && is_callable('getAllHeaders')) 
		{
			$laHeaders = getAllHeaders();
			$paData = ArrayObject::merge($paData, $laHeaders);
			
			$laHeaders = array_change_key_case($laHeaders, CASE_LOWER);
			
			if (isset($laHeaders['authorization'])) 
			{
				$paData['HTTP_AUTHORIZATION'] = $laHeaders['authorization'];
			}
		}
		
		return $paData;
	}
	
	
	/**
	 * Return array of HTTP header names and values.
	 * This method returns the _original_ header name
	 * as specified by the end user.
	 *
	 * @return array
	 */
	public function all () : array
	{
		$laAll = parent::all();
		$laOut = [];
		
		foreach ($laAll as $lsKey => $laProps) 
		{
			$laOut[$laProps['originalKey']] = $laProps['value'];
		}
		
		return $laOut;
	}
	
	
	/**
	 * Set HTTP header value
	 *
	 * This method sets a header value. It replaces
	 * any values that may already exist for the header name.
	 *
	 * @param string $psKey The case-insensitive header name
	 * @param mixed $pmValue The header value
	 */
	public function set (string $psKey, $pmValue) : void
	{
		if (!is_array($pmValue)) 
		{
			$pmValue = [$pmValue];
		}
		
		parent::set($this->normalizeKey($psKey), [
			'value' => $pmValue,
			'originalKey' => $psKey
		]);
	}
	
	
	/**
	 * Get HTTP header value
	 *
	 * @param string $psKey The case-insensitive header name
	 * @param mixed $default The default value if key does not exist
	 * @return string|array
	 */
	public function get (string $psKey, mixed $pmDefault = null)
	{
		if ($this->has($psKey)) 
		{
			return parent::get($this->normalizeKey($psKey))['value'];
		}
		
		return $pmDefault;
	}
	
	
	/**
	 * Get HTTP header key as originally specified
	 *
	 * @param string $psKey The case-insensitive header name
	 * @param mixed $pmDefault The default value if key does not exist
	 * @return mixed
	 */
	public function getOriginalKey (string $psKey, mixed $pmDefault = null)
	{
		if ($this->has($psKey)) 
		{
			return parent::get($this->normalizeKey($psKey))['originalKey'];
		}
		
		return $pmDefault;
	}
	
	
	/**
	 * Add HTTP header value
	 *
	 * This method appends a header value. Unlike the set() method,
	 * this method _appends_ this new value to any values
	 * that already exist for this header name.
	 *
	 * @param string $psKey The case-insensitive header name
	 * @param array|string $pmValue The new header value(s)
	 * @return \OnionHttp\Headers
	 */
	public function add (string $psKey, $pmValue) : Headers
	{
		$laOldValues = $this->get($psKey, []);
		$laValues = is_array($pmValue) ? $pmValue : [$pmValue];
		$this->set($psKey, ArrayObject::merge($laOldValues, array_values($laValues)));
		
		return $this;
	}
	
	
	/**
	 * Does this collection have a given header?
	 *
	 * @param string $psKey The case-insensitive header name
	 * @return bool
	 */
	public function has (string $psKey) : bool
	{
		return parent::has($this->normalizeKey($psKey));
	}
	
	
	/**
	 * Remove header from collection
	 *
	 * @param string $psKey The case-insensitive header name
	 */
	public function remove (string $psKey) : void
	{
		parent::remove($this->normalizeKey($psKey));
	}
	
	
	/**
	 * Normalize header name
	 *
	 * This method transforms header names into a
	 * normalized form. This is how we enable case-insensitive
	 * header names in the other methods in this class.
	 *
	 * @param string $psKey The case-insensitive header name
	 * @return string Normalized header name
	 */
	public function normalizeKey (string $psKey) : string
	{
		$psKey = strtr(strtolower($psKey), '_', '-');
		
		if (strpos($psKey, 'http-') === 0) 
		{
			$psKey = substr($psKey, 5);
		}
		
		return $psKey;
	}
}
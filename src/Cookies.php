<?php
declare (strict_types = 1);

namespace OnionHttp;


/**
 * Cookie helper
 */
class Cookies
{
	/**
	 * Cookies from HTTP request
	 *
	 * @var array
	 */
	protected $aRequestCookies = [];
	
	/**
	 * Cookies for HTTP response
	 *
	 * @var array
	 */
	protected $aResponseCookies = [];
	
	/**
	 * Default cookie properties
	 *
	 * @var array
	 */
	protected $aDefaults = [
			'value' => '',
			'domain' => null,
			'hostonly' => null,
			'path' => '/',
			'expires' => 0,
			'secure' => false,
			'httponly' => false
	];
	
	
	/**
	 * Create new cookies helper
	 *
	 * @param array $paCookies
	 */
	public function __construct (array $paCookies = [])
	{
		$this->aRequestCookies = $paCookies;
	}
	
	
	/**
	 * 
	 * @return \OnionHttp\Cookies
	 */
	public static function create () : Cookies
	{
		return new static($_COOKIE);
	}
	
	
	/**
	 * Set default cookie properties
	 *
	 * @param array $paSettings
	 */
	public function setDefaults (array $paSettings) : void
	{
		foreach ($paSettings as $lsKey => $lmValue)
		{
			if (key_exists($lsKey, $this->aDefaults))
			{
				$this->aDefaults[$lsKey] = $lmValue;
			}
		}
	}
	
	
	/**
	 * Get request cookie
	 *
	 * @param string $psName Cookie name
	 * @param mixed $default Cookie default value
	 * @return mixed Cookie value if present, else default
	 */
	public function getRequestCookie (string $psName, mixed $pmDefault = null)
	{
		return isset($this->aRequestCookies[$psName]) ? $this->aRequestCookies[$psName] : $pmDefault;
	}
	
	
	/**
	 * Get all request cookies
	 *
	 * @return array
	 */
	public function getRequestCookies () : array
	{
		return $this->aRequestCookies;
	}
	
	
	/**
	 * Set response cookie
	 *
	 * @param string $psName Cookie name
	 * @param string|array $pmValue Cookie value, or cookie properties
	 * @return \OnionHttp\Cookies
	 */
	public function set (string $psName, $pmValue) : Cookies
	{
		if (!is_array($pmValue)) 
		{
			$pmValue = ['value' => (string)$pmValue];
		}
		
		$this->aResponseCookies[$psName] = array_replace($this->aDefaults, $pmValue);
		
		return $this;
	}
	
	
	/**
	 * Get response cookie
	 *
	 * @param string $psName Cookie name
	 * @param mixed $pmDefault Cookie default value
	 * @return mixed Cookie value if present
	 */
	public function getResponseCookie (string $psName, mixed $pmDefault = null)
	{
		return isset($this->aResponseCookies[$psName]) ? $this->aResponseCookies[$psName] : $pmDefault;
	}
	
	
	/**
	 * Get all response cookie
	 *
	 * @return array of Cookies
	 */
	public function getResponseCookies () : array
	{
		return $this->aResponseCookies;
	}
	
	
	/**
	 * Convert to `Set-Cookie` headers
	 *
	 * @return array
	 */
	public function toHeaders () : array
	{
		$laHeaders = [];
		
		foreach ($this->aResponseCookies as $lsNname => $laProperties) 
		{
			$laHeaders[] = $this->toHeader($lsNname, $laProperties);
		}
		
		return $laHeaders;
	}
	
	
	/**
	 * Convert to `Set-Cookie` header
	 *
	 * @param string $psName Cookie name
	 * @param array $paProperties Cookie properties
	 * @return string
	 */
	protected function toHeader (string $psName, array $paProperties) : string
	{
		$lsResult = urlencode($psName) . '=' . urlencode($paProperties['value']);
		
		if (isset($paProperties['domain'])) 
		{
			$lsResult .= '; domain=' . $paProperties['domain'];
		}
		
		if (isset($paProperties['path'])) 
		{
			$lsResult .= '; path=' . $paProperties['path'];
		}
		
		if (isset($paProperties['expires'])) 
		{
			if (is_string($paProperties['expires'])) 
			{
				$lnTimestamp = strtotime($paProperties['expires']);
			} 
			else 
			{
				$lnTimestamp = (int)$paProperties['expires'];
			}
			
			if ($lnTimestamp !== 0) 
			{
				$lsResult .= '; expires=' . gmdate('D, d-M-Y H:i:s e', $lnTimestamp);
			}
		}
		
		if (isset($paProperties['secure']) && $paProperties['secure']) 
		{
			$lsResult .= '; secure';
		}
		
		if (isset($paProperties['hostonly']) && $paProperties['hostonly']) 
		{
			$lsResult .= '; HostOnly';
		}
		
		if (isset($paProperties['httponly']) && $paProperties['httponly']) 
		{
			$lsResult .= '; HttpOnly';
		}
		
		return $lsResult;
	}
	
	
	/**
	 * Parse HTTP request `Cookie:` header and extract
	 * into a PHP associative array.
	 *
	 * @param array|string $pmHeader The raw HTTP request `Cookie:` header
	 * @return array Associative array of cookie names and values
	 * @throws \InvalidArgumentException if the cookie data cannot be parsed
	 */
	public static function parseHeader ($pmHeader) : array
	{
		if (is_array($pmHeader)) 
		{
			$pmHeader = isset($pmHeader[0]) ? $pmHeader[0] : '';
		}
		
		if (!is_string($pmHeader)) 
		{
			throw new \InvalidArgumentException('Cannot parse Cookie data. Header value must be a string.');
		}
		
		$pmHeader = rtrim($pmHeader, "\r\n");
		$lsPieces = preg_split('@[;]\s*@', $pmHeader);
		$laCookies = [];
		
		foreach ($lsPieces as $lsCookie) 
		{
			$laCookie = explode('=', $lsCookie, 2);
			
			if (count($laCookie) === 2) 
			{
				$lsKey = urldecode($laCookie[0]);
				$lsValue = urldecode($laCookie[1]);
				
				if (!isset($laCookies[$lsKey])) 
				{
					$laCookies[$lsKey] = $lsValue;
				}
			}
		}
		
		return $laCookies;
	}
	
	
	/**
	 * 
	 * @param string|null $psName
	 */
	public function register (?string $psName = null) : void
	{
		if (!is_null($psName))
		{
			$laCookies[$psName] = $this->getResponseCookie($psName);
		}
		else 
		{
			$laCookies = $this->getResponseCookies();
		}

		if (is_array($laCookies))
		{
			foreach ($laCookies as $lsName => $laProp)
			{
				if (is_array($laProp))
				{
					setcookie(
						$lsName,
						$laProp['value'],
						$laProp['expires'],
						(string)$laProp['path'],
						(string)$laProp['domain'],						
						(bool)$laProp['secure'],
						(bool)$laProp['httponly']
					);
				}
			}
		}
	}
}
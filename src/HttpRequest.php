<?php
declare (strict_types = 1);

namespace OnionHttp;
use OnionHttp\Cookies;
use OnionHttp\Headers;
use OnionHttp\ServerRequest;
use OnionHttp\Stream;
use OnionHttp\Uri;
use OnionLib\ArrayObject;
use OnionLib\Collection;
use OnionLib\Debug;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;


class HttpRequest extends ServerRequest
{
	
	/**
	 * 
	 * @return \OnionHttp\HttpRequest
	 */
	public static function factory () : HttpRequest
	{
		if (PHP_SAPI == "cli")
		{
			return self::createConsoleRequest();
		}
		else 
		{
			return self::createServerRequest();
		}
	}
	
	
	/**
	 * Create new HTTP request with data extracted from the server environment
	 *
	 * @return \OnionHttp\HttpRequest
	 */
	public static function createServerRequest () : HttpRequest
	{
		$lsMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';
		$loUri = new Uri();
		$loHeaders = new Headers();
		$loCookies = new Cookies($_COOKIE);
		$loBody = new Stream();
		$laUploadedFiles = UploadedFile::factory($_FILES);
		
		$loRequest = new static($lsMethod, $loUri, $loHeaders, $loCookies, $_SERVER, $loBody, $laUploadedFiles);
		
		$loRequest->withQueryParams($_GET);
		
		if ($lsMethod === 'POST')
		{
			$loRequest = $loRequest->withPostParams($_POST);
		}
		
		$loRequest->setDefaltMediaTypeParser();
		$loRequest->getParsedBody();
		
		return $loRequest;
	}
	
	
	/**
	 * Create new HTTP request with data extracted from the prompt environment
	 *
	 * @return \OnionHttp\HttpRequest
	 */
	public static function createConsoleRequest () : HttpRequest
	{
		$laArgv = self::parseArgv();
		
		$loUri = new Uri($laArgv['uri']);
		$loHeaders = new Headers($laArgv['header']);
		$loCookies = new Cookies();
		$loBody = new Stream();
		$loBody->write($laArgv['body']);
		
		$loRequest = new static($laArgv['method'], $loUri, $loHeaders, $loCookies, $_SERVER, $loBody);
		
		$loRequest->withArgvParams($laArgv['params']);
		
		$loRequest->setDefaltMediaTypeParser();
		$loRequest->getParsedBody();
		
		return $loRequest;
	}
	
	
	/**
	 *
	 * return array
	 */
	public static function parseArgv () : array
	{
		$lsUri = 'http://localhost/';
		$laArgv['method'] = 'GET';
		$laArgv['uri'] = '';
		$laArgv['header'] = [];
		$laArgv['params'] = [];
		$laArgv['body'] = '';

		$lsAction = '';
		
		Debug::debug($_SERVER['argv']);
		
		if (isset($_SERVER['argv']) && is_array($_SERVER['argv']))
		{
			foreach ($_SERVER['argv'] as $lsArg)
			{
				$laArg = explode("=", $lsArg);
				
				switch ($laArg[0])
				{
					case '--method':
					case '--M':
					case '-M':
						$laArgv['method'] = $laArg[1];
						break;
					case '--header':
					case '--H':
					case '-H':
						$laHeader = explode(":", $laArg[1]);
						$laArgv['header'][$laHeader[0]] = $laHeader[1];
						break;
					case '--uri':
					case '--u':
					case '-u':
						$laArgv['uri'] = $lsUri . $laArg[1];
						break;
					case '--module':
					case '--m':
					case '-m':
						$lsModule = $laArg[1];
						break;
					case '--controller':
					case '--c':
					case '-c':
						$lsController = $laArg[1];
						break;
					case '--action':
					case '--a':
					case '-a':
						$lsAction = $laArg[1];
						break;
					case '--body':
					case '--b':
					case '-b':
						$laArgv['body'] = $laArg[1];
						break;
					default:
						if (isset($laArg[1]))
						{
							$laArgv['params'][$laArg[0]] = $laArg[1];
						}
						break;
				}
			}
			
			if (empty($laArgv['uri']))
			{
				if (empty($lsModule))
				{
					$lsModule = $lsAction;
				}
				
				if (empty($lsController))
				{
					$lsController = $lsModule;
				}
				
				if (empty($lsAction))
				{
					$lsAction = $lsController;
				}
				
				if (empty($lsModule) && empty($lsController) && empty($lsAction))
				{
					$lsModule = $lsController = $lsAction = 'index';
				}
				
				$laArgv['uri'] = $lsUri;
				$laArgv['uri'] .= (isset($lsModule) ? "{$lsModule}/" : "");
				$laArgv['uri'] .= (isset($lsController) ? "{$lsController}/" : "");
				$laArgv['uri'] .= (isset($lsAction) ? "{$lsAction}/" : "");
			}
		}
		
		Debug::debug($laArgv);
		
		return $laArgv;
	}	
	

	/**
	 * Create new HTTP request.
	 *
	 * Adds a host header when none was provided and a host is defined in uri.
	 *
	 * @param string $psMethod The request method
	 * @param UriInterface $poUri The request URI object
	 * @param \OnionHttp\Headers $poHeaders The request headers collection
	 * @param \OnionHttp\Cookies $poCookies The request cookies collection
	 * @param array $paServerParams The server environment variables
	 * @param StreamInterface $poBody The request body object
	 * @param array $paUploadedFiles The request uploadedFiles collection
	 * @throws InvalidArgumentException on invalid HTTP method
	 */
	public function __construct (string $psMethod, UriInterface $poUri, Headers $poHeaders, Cookies $poCookies, array $paServerParams, StreamInterface $poBody, array $paUploadedFiles = [])
	{
		$this->sMethod = $this->validMethod($psMethod);
		$this->oUri = $poUri;
		$this->oHeaders = $poHeaders;
		$this->oCookies = $poCookies;
		$this->aServerParams = $paServerParams;
		$this->oAttributes = new Collection();
		$this->oBody = $poBody;
		$this->aUploadedFiles = $paUploadedFiles;
		
		$this->getRequestTarget();
		
		if (isset($paServerParams['SERVER_PROTOCOL']))
		{
			$this->protocolVersion = str_replace('HTTP/', '', $paServerParams['SERVER_PROTOCOL']);
		}
		
		if (!$this->oHeaders->has('Host') || $this->oUri->getHost() !== '')
		{
			$this->oHeaders->set('Host', $this->oUri->getHost());
		}
	}
	
	
	/**
	 * This method is applied to the cloned object
	 * after PHP performs an initial shallow-copy. This
	 * method completes a deep-copy by creating new objects
	 * for the cloned object's internal reference pointers.
	 */
	public function __clone ()
	{
		$this->oHeaders = clone $this->oHeaders;
		$this->oCookies = clone $this->oCookies;
		$this->oAttributes = clone $this->oAttributes;
		$this->oBody = clone $this->oBody;
	}
	
	
	/**
	 *
	 * @return \OnionHttp\HttpRequest|\OnionLib\Collection|\SimpleXMLElement|null
	 */
	public function setDefaltMediaTypeParser ()
	{
		$this->registerMediaTypeParser('application/json', function ($poInput) {
			$laResult = json_decode($poInput, true);
			
			if (!is_array($laResult))
			{
				return null;
			}
			
			return new Collection($laResult);
		});
			
		$this->registerMediaTypeParser('application/xml', function ($poInput) {
			$lbBackup = libxml_disable_entity_loader(true);
			$lbBackupErrors = libxml_use_internal_errors(true);
			$loResult = simplexml_load_string($poInput);
			libxml_disable_entity_loader($lbBackup);
			libxml_clear_errors();
			libxml_use_internal_errors($lbBackupErrors);
				
			if (!is_object($loResult))
			{
				return null;
			}
			
			return $loResult;
		});
				
		return $this;
	}
	
	
	/**
	 * Retrieve a server parameter.
	 *
	 * @param string $psKey
	 * @param mixed $pmDefault
	 * @return mixed
	 */
	public function getUploadedFile (string $psKey, mixed $pmDefault = null)
	{
		return isset($this->aUploadedFiles[$psKey]) ? $this->aUploadedFiles[$psKey] : $pmDefault;
	}
	
	
	/**
	 * Retrieve a server parameter.
	 *
	 * @param string $psKey
	 * @param mixed $pmDefault
	 * @return mixed
	 */
	public function getServerParam (string $psKey, mixed $pmDefault = null)
	{
		return isset($this->aServerParams[$psKey]) ? $this->aServerParams[$psKey] : $pmDefault;
	}
	
	
	/**
	 * Fetch cookie value from cookies sent by the client to the server.
	 *
	 * @param string $psKey The attribute name.
	 * @param mixed  $pmDefault Default value to return if the attribute does not exist.
	 * @return mixed
	 */
	public function getCookieParam (string $psKey, mixed $pmDefault = null)
	{
		return $this->oCookies->getRequestCookie($psKey, $pmDefault);
	}
	
	
	/**
	 * Retrieve post params.
	 *
	 * Retrieves the deserialized post params, if any.
	 *
	 * @return array
	 */
	public function getPostParams () : array
	{
		if (is_object($this->oPostParams))
		{
			return $this->oPostParams->all();
		}
		
		return [];
	}
	
	
	/**
	 * Return an instance with the specified post params.
	 *
	 * These values SHOULD remain immutable over the course of the incoming
	 * request. They MAY be injected during instantiation, such as from PHP's
	 * $_POST superglobal, or MAY be derived from some other value such as the
	 * URI. In cases where the arguments are parsed from the URI, the data
	 * MUST be compatible with what PHP's parse_str() would return for
	 * purposes of how duplicate query parameters are handled, and how nested
	 * sets are handled.
	 *
	 * Setting query string arguments MUST NOT change the URI stored by the
	 * request, nor the values in the server params.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * updated query string arguments.
	 *
	 * @param array $paPost Array of post params, typically from
	 *     $_POST.
	 * @return \OnionHttp\HttpRequest
	 */
	public function withPostParams (array $paPost) : HttpRequest
	{
		$this->oPostParams = new Collection($paPost);
		
		return $this;
	}
	
	
	/**
	 * Retrieve args params.
	 *
	 * Retrieves the deserialized args params, if any.
	 *
	 * @return array
	 */
	public function getArgvParams () : array
	{
		if (is_object($this->oArgvParams))
		{
			return $this->oArgvParams->all();
		}
		
		return [];
	}
	
	
	/**
	 * Return an instance with the specified args params.
	 *
	 * These values SHOULD remain immutable over the course of the incoming
	 * request. They MAY be injected during instantiation, such as from PHP's
	 * $_SERVER[ARGS] superglobal, or MAY be derived from some other value such as the
	 * URI. In cases where the arguments are parsed from the URI, the data
	 * MUST be compatible with what PHP's parse_str() would return for
	 * purposes of how duplicate query parameters are handled, and how nested
	 * sets are handled.
	 *
	 * Setting query string arguments MUST NOT change the URI stored by the
	 * request, nor the values in the server params.
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return an instance that has the
	 * updated query string arguments.
	 *
	 * @param array $paArgs Array of args params, typically from
	 *     $_SERVER[ARGS].
	 * @return \OnionHttp\HttpRequest
	 */
	public function withArgvParams (array $paArgs) : HttpRequest
	{
		$this->oArgvParams = new Collection($paArgs);
		
		return $this;
	}

	
	/**
	 * Create a new instance with the specified derived request attributes.
	 *
	 * This method allows setting all new derived request attributes as
	 * described in getAttributes().
	 *
	 * This method MUST be implemented in such a way as to retain the
	 * immutability of the message, and MUST return a new instance that has the
	 * updated attributes.
	 *
	 * @param array $paAttributes New attributes
	 * @return \OnionHttp\HttpRequest
	 */
	public function withAttributes (array $paAttributes) : HttpRequest
	{
		$this->oAttributes = new Collection($paAttributes);
		
		return $this;
	}
	
	
	/**
	 * Does this request use a given method?
	 *
	 * @param string $psMethod HTTP method
	 * @return bool
	 */
	public function isMethod (string $psMethod) : bool
	{
		return $this->getMethod() === $psMethod;
	}
	
	
	/**
	 * Is this a GET request?
	 *
	 * @return bool
	 */
	public function isGet () : bool
	{
		return $this->isMethod('GET');
	}
	
	
	/**
	 * Is this a POST request?
	 *
	 * @return bool
	 */
	public function isPost () : bool
	{
		return $this->isMethod('POST');
	}
	
	
	/**
	 * Is this a PUT request?
	 *
	 * @return bool
	 */
	public function isPut () : bool
	{
		return $this->isMethod('PUT');
	}
	
	
	/**
	 * Is this a PATCH request?
	 *
	 * @return bool
	 */
	public function isPatch () : bool
	{
		return $this->isMethod('PATCH');
	}
	
	
	/**
	 * Is this a DELETE request?
	 *
	 * @return bool
	 */
	public function isDelete () : bool
	{
		return $this->isMethod('DELETE');
	}
	
	
	/**
	 * Is this a HEAD request?
	 *
	 * @return bool
	 */
	public function isHead () : bool
	{
		return $this->isMethod('HEAD');
	}
	
	
	/**
	 * Is this a OPTIONS request?
	 *
	 * @return bool
	 */
	public function isOptions () : bool
	{
		return $this->isMethod('OPTIONS');
	}
	
	
	/**
	 * Is this an XHR request?
	 *
	 * @return bool
	 */
	public function isXhr () : bool
	{
		$lsHeader = $this->getHeader('X-Requested-With');
		
		if (is_string($lsHeader) && $lsHeader === 'XMLHttpRequest')
		{
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Get request media type params, if known.
	 *
	 * @return array
	 */
	public function getMediaTypeParams () : array
	{
		$lsContentType = $this->getContentType();
		$laContentTypeParams = [];
		
		if ($lsContentType)
		{
			$laContentTypeParts = preg_split('/\s*[;,]\s*/', $lsContentType);
			$lnContentTypePartsLength = count($laContentTypeParts);
			
			for ($lnI = 1; $lnI< $lnContentTypePartsLength; $lnI++)
			{
				$laParamParts = explode('=', $laContentTypeParts[$lnI]);
				$laContentTypeParams[strtolower($laParamParts[0])] = $laParamParts[1];
			}
		}
		
		return $laContentTypeParams;
	}
	
	
	/**
	 * Get request content character set, if known.
	 *
	 * @return string|null
	 */
	public function getContentCharset ()
	{
		$laMediaTypeParams = $this->getMediaTypeParams();
		
		if (isset($laMediaTypeParams['charset']))
		{
			return $laMediaTypeParams['charset'];
		}
		
		return null;
	}
	
	
	/**
	 * Get request content length, if known.
	 *
	 * @return int|null
	 */
	public function getContentLength () : int
	{
		$laResult = $this->oHeaders->get('Content-Length');
		
		return $laResult ? (int)$laResult[0] : null;
	}
	
	
	/**
	 * Fetch request parameter value from body or query string (in that order).
	 *
	 * @param string $psKey The parameter key.
	 * @param string $pmDefault The default value.
	 * @return mixed The parameter value.
	 */
	public function getParam (string $psKey, mixed $pmDefault= null)
	{
		$lmPostParams = $this->getParsedBody();
		$laGetParams = $this->getQueryParams();
		$lmResult = $pmDefault;
		
		if (is_array($lmPostParams) && isset($lmPostParams[$psKey]))
		{
			$lmResult= $lmPostParams[$psKey];
		}
		elseif (is_object($lmPostParams) && property_exists($lmPostParams, $psKey))
		{
			$lmResult= $lmPostParams->$psKey;
		}
		elseif (isset($laGetParams[$psKey]))
		{
			$lmResult= $laGetParams[$psKey];
		}
		
		return $lmResult;
	}
	
	
	/**
	 * Fetch parameter value from request body.
	 *
	 * @param string $psKey
	 * @param mixed $pmDefault
	 * @return mixed
	 */
	public function getParsedBodyParam (string $psKey, mixed $pmDefault= null)
	{
		$lmPostParams= $this->getParsedBody();
		$lmResult= $pmDefault;
		
		if (is_array($lmPostParams) && isset($lmPostParams[$psKey]))
		{
			$lmResult= $lmPostParams[$psKey];
		}
		elseif (is_object($lmPostParams) && property_exists($lmPostParams, $psKey))
		{
			$lmResult= $lmPostParams->$psKey;
		}
		
		return $lmResult;
	}
	
	
	/**
	 * Fetch parameter value from query string.
	 *
	 * @param string $psKey
	 * @param mixed $pmDefault
	 * @return mixed
	 */
	public function getQueryParam (string $psKey, mixed $pmDefault = null)
	{
		$laGetParams= $this->getQueryParams();
		$lmResult= $pmDefault;
		
		if (isset($laGetParams[$psKey]))
		{
			$lmResult= $laGetParams[$psKey];
		}
		
		return $lmResult;
	}
	
	
	/**
	 * Fetch associative array of body and query string parameters.
	 *
	 * @return array
	 */
	public function getParams () : array
	{
		$laParams = $this->getQueryParams();
		$laPostParams = $this->getParsedBody();
		
		if ($laPostParams)
		{
		    $laParams = ArrayObject::merge($laParams, (array)$laPostParams, true);
		}
		
		return $laParams;
	}
	
	
	/**
	 * Return the whole object and its children as an array
	 *
	 * @return array
	 */
	public function toArray () : array
	{
		$laProperties = get_object_vars($this);
		
		if (is_array($laProperties))
		{
			foreach ($laProperties as $lsVar => $lmValue)
			{
				$lsKey = substr($lsVar, 1);
				
				if (is_array($lmValue) && count($lmValue) != 0)
				{
					foreach ($lmValue as $lsId => $lmObj)
					{
						if (is_object($lmObj) && method_exists($lmObj, 'get'))
						{
							$laReturn[$lsKey][$lsId] = $lmObj->get();
						}
						else
						{
							$laReturn[$lsKey][$lsId] = $lmObj;
						}
					}
				}
				else
				{
					$laReturn[$lsKey] = $lmValue;
				}
			}
		}
		
		return $laReturn;
	}
}